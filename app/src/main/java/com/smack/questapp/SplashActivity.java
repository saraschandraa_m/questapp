package com.smack.questapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import aynctask.WS_Category;
import aynctask.WS_Login;
import listeners.TokenRegisterListener;
import model.CategoryItem;
import model.UserInfo;
import utils.CatchJSONExceptions;
import utils.Constants;
import utils.Logger;
import utils.WSLoadingDialog;

//import com.google.android.gms.gcm.GoogleCloudMessaging;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity implements TokenRegisterListener {


    @ViewById(R.id.iv_splashicon)
    ImageView mIvSplashIcon;

    @ViewById(R.id.ll_loginoption)
    LinearLayout mllLoginOptions;

    @ViewById(R.id.ll_progresslayout)
    LinearLayout mllProgressLayout;

    Context mContext;
    SharedPreferences mAppPref;
    SharedPreferences.Editor mLoginEditor;
    CatchJSONExceptions mCatchJSONExceptions;
    UserInfo userInfo;
    CallbackManager mCallbackManager;
    Handler splashHandler;
    WSLoadingDialog wsLoadingDialog;

    String userResult, userID, sEmail, sFacebookId, sRegId;
    boolean isLogin;

    ArrayList<CategoryItem> categoryItems;

//    GoogleCloudMessaging gcmObj;

    @AfterViews
    void onViewLoaded() {
        mCatchJSONExceptions = new CatchJSONExceptions();
        wsLoadingDialog = new WSLoadingDialog(mContext);
        categoryItems = new ArrayList<>();
        splashHandler = new Handler();

        mAppPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        mLoginEditor = mAppPref.edit();
        isLogin = mAppPref.getBoolean(Constants.PREF_ISLOGGED, false);
        userResult = mAppPref.getString(Constants.PREF_USERDETAILS, "");
        sRegId = mAppPref.getString(Constants.PREG_GCMREGID, "");
        if (userResult != null && !userResult.isEmpty()) {
            JSONObject result = new JSONObject();
            result = mCatchJSONExceptions.getJSONFromString(userResult);
            userInfo = new UserInfo(result);
            userID = userInfo.getUser_id();
            ApplicationEx.getAppInstance().setmUserInfo(userInfo);
        }

        if (isNetworkAvailable(mContext)) {
            if (sRegId.isEmpty()) {
                ApplicationEx.getAppInstance().setsGCMRegID(sRegId);
                registerInBackground();
            } else {
                initializeApp();
            }
        } else {
            AlertDialog.Builder internetDialog = new AlertDialog.Builder(mContext);
            internetDialog.setTitle("No Internet");
            internetDialog.setMessage("Please enable internet and try again");
            internetDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            internetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finish();
                }
            });
            internetDialog.show();
        }
        ApplicationEx.getAppInstance().setTokenRegisterListener(this);
    }


    @Click(R.id.ll_facebooklogin)
    void onFBLoginClicked() {
        wsLoadingDialog.showWSLoadingDialog();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email", "user_location"));
    }

    @Click(R.id.ll_signin)
    void onSignInClicked() {
        startActivity(new Intent(SplashActivity.this, LoginActivity_.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.smack.questapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        FacebookSdk.sdkInitialize(mContext);
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logger.showInfo("Success", "Login");
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("Facebook Response", object.toString());
                                sEmail = mCatchJSONExceptions.getStringFromJSON(object, "email");
                                sFacebookId = mCatchJSONExceptions.getStringFromJSON(object, "id");
                                signInwithFB(sEmail, false);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday,picture.type(large),location");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(mContext, "Login Cancel", Toast.LENGTH_LONG).show();
                        Logger.showInfo("FBLogin", "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(mContext, exception.getMessage(), Toast.LENGTH_LONG).show();
                        Logger.showInfo("FBLogin", "Error");
                    }
                });
    }

    private void signInwithFB(String email, final boolean isFromRegistration) {
        sEmail = email;
        WS_Login wsLogin = new WS_Login(mContext);
        wsLogin.doFBLogin(sEmail, ApplicationEx.getAppInstance().getsGCMRegID(), new WS_Login.LoginListener() {
            @Override
            public void onLoginCompleted(boolean isSucess, UserInfo mResponse) {
                if (!isSucess && mResponse.getError().equals("0WCE02")) {
                    wsLoadingDialog.hideWSLoadingDialog();
                    moveToRegistrationFromFB(sEmail);
                } else {
                    if (!isFromRegistration) {
                        doGetCategory(mResponse.getUser_id());
                    } else {
                        Intent profileImgIntent = new Intent(SplashActivity.this, ProfileImageActivity_.class);
                        profileImgIntent.putExtra(Constants.KEY_USERID, userID);
                        startActivity(profileImgIntent);
                        finish();
                    }
                }
            }
        });

    }

    private void doGetCategory(String sUserID) {
        userID = sUserID;
        WS_Category wsCategory = new WS_Category(mContext);
        wsCategory.doGetCategory(userID, new WS_Category.CategoryListener() {
            @Override
            public void onCategoryReceived(boolean isSuccess, ArrayList<CategoryItem> categoryList) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (isSuccess) {
                    categoryItems = categoryList;
                    moveToCategory();
                }
            }
        });
    }

    private void initializeApp() {
        if (isLogin) {
            mllLoginOptions.setVisibility(View.GONE);
            mllProgressLayout.setVisibility(View.VISIBLE);
            doGetCategory(userID);
        } else {
            mllLoginOptions.setVisibility(View.VISIBLE);
            mllProgressLayout.setVisibility(View.GONE);
        }
    }

    private void moveToCategory() {
        Intent loginIntent = new Intent(SplashActivity.this, CategoryActivity_.class);
        loginIntent.putExtra(Constants.KEY_CATEGORY, categoryItems);
        startActivity(loginIntent);
        finish();
    }

    private void moveToRegistrationFromFB(String sEmail) {
        Intent registrationIntent = new Intent(SplashActivity.this, RegisterActivity_.class);
        registrationIntent.putExtra(Constants.KEY_EMAIL, sEmail);
        registrationIntent.putExtra(Constants.KEY_FBID, sFacebookId);
        registrationIntent.putExtra(Constants.KEY_FROMFB, true);
        startActivityForResult(registrationIntent, 0);
    }

    private void startSplashAnimation() {
        Animation headerAnim = AnimationUtils.loadAnimation(mContext, R.anim.image_anim);
        final Animation animationZoomIn = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in);
        mIvSplashIcon.startAnimation(headerAnim);

        headerAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mllLoginOptions.setVisibility(View.VISIBLE);
                mllLoginOptions.startAnimation(animationZoomIn);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Handler loginHandler = new Handler();
                loginHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                sEmail = data.getExtras().getString("emailid");
                signInwithFB(sEmail, true);
            }
        }

    }


    private void registerInBackground() {
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                mllLoginOptions.setVisibility(View.GONE);
//                mllProgressLayout.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            protected String doInBackground(Void... params) {
//                String msg = "";
//                try {
//                    if (gcmObj == null) {
//                        gcmObj = GoogleCloudMessaging.getInstance(mContext);
//                    }
//                    sRegId = gcmObj.register(Constants.GOOGLE_PROJ_ID);
//                    msg = "Registration ID :" + sRegId;
//                    Log.i("GCMREG ID", msg);
//                } catch (IOException ex) {
//                    msg = "Error :" + ex.getMessage();
//                }
//                return msg;
//            }
//
//            @Override
//            protected void onPostExecute(String msg) {
//                if (!TextUtils.isEmpty(sRegId)) {
//                    Log.i("GCM Sucess", msg);
//                    mLoginEditor.putString("gcmregid", sRegId);
//                    mLoginEditor.commit();
//                    ApplicationEx.getAppInstance().setsGCMRegID(sRegId);
//                    initializeApp();
//                } else {
//                    Toast.makeText(mContext, "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time." + msg, Toast.LENGTH_LONG).show();
//                }
//            }
//        }.execute(null, null, null);
        startService(new Intent(SplashActivity.this, MyFirebaseInstanceIDService.class));
    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onTokenRegistered(String strRegId) {
        // ToDo after Token Registration.
        sRegId = strRegId;
        if (!TextUtils.isEmpty(sRegId)) {
            Log.i("GCM Sucess", sRegId);
            mLoginEditor.putString(Constants.PREG_GCMREGID, sRegId);
            mLoginEditor.commit();
            ApplicationEx.getAppInstance().setsGCMRegID(sRegId);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initializeApp();
                }
            });
        } else {
            Toast.makeText(mContext, "Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time.", Toast.LENGTH_LONG).show();
        }
    }
}
