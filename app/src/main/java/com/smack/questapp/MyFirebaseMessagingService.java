package com.smack.questapp;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import utils.Logger;

/**
 * Created by SaraschandraaM on 16/12/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        Logger.showInfo("Firebase Token", "From: " + remoteMessage.getFrom());
    }
}
