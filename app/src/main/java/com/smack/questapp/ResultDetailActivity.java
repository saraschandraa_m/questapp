package com.smack.questapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import model.UserResultObject;
import utils.Constants;
import utils.Tools;

/**
 * Created by SaraschandraaM on 10/11/16.
 */

@EActivity(R.layout.activity_resultdetail)
public class ResultDetailActivity extends AppCompatActivity {

    @ViewById(R.id.tv_resultdet_quizname)
    TextView mTvQuizname;

    @ViewById(R.id.tv_resultdet_date)
    TextView mTvDate;

    @ViewById(R.id.tv_resultdet_noofques)
    TextView mTvNoofQuestions;

    @ViewById(R.id.tv_resultdet_duration)
    TextView mTvDuration;

    @ViewById(R.id.tv_resultdet_quesanswered)
    TextView mTvQuestionsAnswered;

    @ViewById(R.id.tv_resultdet_timetaken)
    TextView mTvTimeTaken;

    @ViewById(R.id.tv_resultdet_correctques)
    TextView mTvNoofQuestionsCorrect;


    Bundle args;
    Context mContext;
    UserResultObject resultDetail;

    @AfterViews
    void onViewLoaded() {
        args = getIntent().getExtras();
        if (args != null) {
            resultDetail = (UserResultObject) args.getSerializable(Constants.KEY_USERRESULTOBJ);
        }

        setDetailToScreen();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Click(R.id.vw_left)
    void onLeftContentClicked() {
        finish();
    }

    @Click(R.id.vw_right)
    void onRightContentClicked() {
        finish();
    }

    @Click(R.id.ll_profviewcontent)
    void onContentAreaClicked() {
        finish();
    }

    private void setDetailToScreen() {
        String[] dateArr = resultDetail.getCreated_at().split(" ");
        String[] takenDate = Tools.getFormattedProfileDate(dateArr[0]);
        mTvQuizname.setText(resultDetail.getQuiz_name());
        mTvDate.setText(takenDate[0] + " " + takenDate[1] + " " + takenDate[2]);
        mTvNoofQuestions.setText(resultDetail.getNo_of_questions() + "");
        mTvDuration.setText(resultDetail.getDuration() + " mins");
        mTvQuestionsAnswered.setText(resultDetail.getNo_of_question_attended() + "");
        mTvNoofQuestionsCorrect.setText(resultDetail.getNo_of_correct() + "");
        mTvTimeTaken.setText(resultDetail.getTotal_time() + "");
    }
}
