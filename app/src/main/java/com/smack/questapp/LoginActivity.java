package com.smack.questapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import aynctask.WS_Category;
import aynctask.WS_Login;
import aynctask.WebServiceTask;
import listeners.WSListener;
import model.CategoryItem;
import model.ResponseObject;
import model.UserInfo;
import utils.CatchJSONExceptions;
import utils.Constants;
import utils.Urls;
import utils.WSLoadingDialog;

/**
 * Created by KMac on 6/8/16.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById(R.id.txtForgotPassword)
    TextView txtForgotPassword;

    @ViewById(R.id.txtSignUp)
    TextView txtSignup;

    @ViewById(R.id.edtPasswordSignIn)
    EditText mEdtPassword;

    @ViewById(R.id.edtEmailIdSignIn)
    EditText mEdtEmailId;

    Context mContext;
    WSLoadingDialog wsLoadingDialog;

    String mStrEmailId, mStrPassword;
    CatchJSONExceptions mCatchJSONExceptions;

    SharedPreferences loginPref;
    SharedPreferences.Editor loginEditor;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    private GoogleApiClient client;

    @AfterViews
    void viewLoad() {
        mCatchJSONExceptions = new CatchJSONExceptions();
        wsLoadingDialog = new WSLoadingDialog(mContext);
        loginPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        loginEditor = loginPref.edit();

        SpannableString forgotPass = new SpannableString(getString(R.string.forgot_password));
        forgotPass.setSpan(new UnderlineSpan(), 0, forgotPass.length(), 0);
        txtForgotPassword.setText(forgotPass);

        SpannableString signUP = new SpannableString(getString(R.string.sign_up));
        signUP.setSpan(new UnderlineSpan(), 0, signUP.length(), 0);
        txtSignup.setText(signUP);
    }

    @CheckedChange(R.id.ch_showpwd)
    void onShowPwdClicked(boolean isChecked, CompoundButton button) {
        if (!isChecked) {
            mEdtPassword.setInputType(129);
        } else {
            mEdtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Click(R.id.txtSignUp)
    void signUpClicked() {
        Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity_.class);
        startActivityForResult(registerIntent, 0);
    }

    @Click(R.id.txtSignIn)
    void singInClicked() {
        mStrEmailId = mEdtEmailId.getText().toString().trim();
        mStrPassword = mEdtPassword.getText().toString().trim();
        doSignIn(false);
    }


    private void doSignIn(final boolean isFromRegistration) {
        if (mStrEmailId.isEmpty()) {
            showToast(getResources().getString(R.string.empty_email_id));
            return;
        } else if (mStrPassword.isEmpty()) {
            showToast(getResources().getString(R.string.empty_password));
            return;
        }
        wsLoadingDialog.showWSLoadingDialog();
        WS_Login wsLogin = new WS_Login(mContext);
        wsLogin.doLogin(mStrEmailId, mStrPassword, ApplicationEx.getAppInstance().getsGCMRegID(), new WS_Login.LoginListener() {
            @Override
            public void onLoginCompleted(boolean isSucess, UserInfo mResponse) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (isSucess) {
                    if (!isFromRegistration) {
                        ApplicationEx.getAppInstance().setsUserId(mResponse.getUser_id());
                        doGetCategory(mResponse.getUser_id());
                    } else {
                        moveToProfileImage(mResponse.getUser_id());
                    }
                } else {
                    Toast.makeText(mContext, "Incorrect Email or Password.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void doGetCategory(String userID) {
        WS_Category wsCategory = new WS_Category(mContext);
        wsCategory.doGetCategory(userID, new WS_Category.CategoryListener() {
            @Override
            public void onCategoryReceived(boolean isSuccess, ArrayList<CategoryItem> categoryList) {
                if (isSuccess) {
                    Intent loginIntent = new Intent(LoginActivity.this, CategoryActivity_.class);
                    loginIntent.putExtra(Constants.KEY_CATEGORY, categoryList);
                    startActivity(loginIntent);
                    finish();
                }
            }
        });
    }

    private void moveToProfileImage(String userID) {
        Intent profileImgIntent = new Intent(LoginActivity.this, ProfileImageActivity_.class);
        profileImgIntent.putExtra(Constants.KEY_USERID, userID);
        startActivity(profileImgIntent);
        finish();
    }

    private void doGetQuestions(String userID) {
        String params = Urls.QUESTIONS + userID + Urls.QUIZ_CATEGORY_ID + "1";
        new WebServiceTask(mContext, params, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {

            }
        }).execute();
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                mStrEmailId = data.getExtras().getString("emailid");
                mStrPassword = data.getExtras().getString("pwd");
                mEdtEmailId.setText(mStrEmailId);
                mEdtPassword.setText(mStrPassword);
                doSignIn(true);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "Login Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app URL is correct.
//                Uri.parse("android-app://com.smack.questapp/http/host/path")
//        );
//        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "Login Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app URL is correct.
//                Uri.parse("android-app://com.smack.questapp/http/host/path")
//        );
//        AppIndex.AppIndexApi.end(client, viewAction);
//        client.disconnect();
    }
}
