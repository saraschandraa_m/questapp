package com.smack.questapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

import adapter.PlacesAutoCompleteAdapter;
import aynctask.WS_Register;
import aynctask.WebServiceTask;
import listeners.WSListener;
import model.ResponseObject;
import utils.CatchJSONExceptions;
import utils.Constants;
import utils.Urls;
import utils.WSLoadingDialog;

/**
 * Created by KMac on 6/8/16.
 */

@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {

    @ViewById(R.id.toolbarRegistration)
    Toolbar mToolBar;

    @ViewById(R.id.edtEmailId)
    EditText edtEmailIld;

    @ViewById(R.id.edtPassword)
    EditText edtPassword;

    @ViewById(R.id.edtConfirmPassword)
    EditText edtConfirmPassword;

    @ViewById(R.id.edtName)
    EditText edtName;

    @ViewById(R.id.edtLastName)
    EditText edtLastName;

    @ViewById(R.id.edtGender)
    EditText edtGender;

    @ViewById(R.id.edtDOB)
    EditText edtDOB;

    @ViewById(R.id.edtPhoneNumber)
    EditText edtPhoneNumber;

    @ViewById(R.id.edtCollegeName)
    EditText edtCollegeName;

    @ViewById(R.id.ll_password)
    LinearLayout mLlPassword;

    @ViewById(R.id.ll_confirmpwd)
    LinearLayout mLlConfirmPwd;

    @ViewById(R.id.edtLocation)
    AutoCompleteTextView edtLocation;

    Context mContext;
    Bundle arguments;
    CatchJSONExceptions mCatchJSONExceptions;
    WSLoadingDialog wsLoadingDialog;
    String strEmailId, strPassword, strConfirmPassword, strGender, strDOB, strDisplayDOB, strFacebookId;
    String[] genderArray = new String[]{"Male", "Female"};

    boolean isGenderSelected, isFromFB;

    int genderPosition;
    Handler mThreadHandler;
    HandlerThread mHandlerThread;

    PlacesAutoCompleteAdapter mAdapter;


    @AfterViews
    void viewLoad() {
        arguments = getIntent().getExtras();
        wsLoadingDialog = new WSLoadingDialog(mContext);
        mToolBar.setTitle("");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        if (arguments != null) {
            strEmailId = arguments.getString(Constants.KEY_EMAIL, "");
            strFacebookId = arguments.getString(Constants.KEY_FBID, "");
            strPassword = "samplepassword";
            strConfirmPassword = "samplepassword";
            isFromFB = arguments.getBoolean(Constants.KEY_FROMFB, false);
        }

        if (isFromFB) {
            edtEmailIld.setVisibility(View.GONE);
            mLlPassword.setVisibility(View.GONE);
            mLlConfirmPwd.setVisibility(View.GONE);
        }

        mCatchJSONExceptions = new CatchJSONExceptions();

        registerForContextMenu(edtGender);

        mAdapter = new PlacesAutoCompleteAdapter(mContext, R.layout.cell_auto_complete);
        edtLocation.setImeActionLabel(getResources().getString(R.string.accept), EditorInfo.IME_ACTION_DONE);
        edtLocation.setAdapter(mAdapter);

        if (mThreadHandler == null) {
            // Initialize and start the HandlerThread
            // which is basically a Thread with a Looper
            // attached (hence a MessageQueue)
            mHandlerThread = new HandlerThread("REGISTER", android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mHandlerThread.start();

            // Initialize the Handler
            mThreadHandler = new Handler(mHandlerThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<String> results = mAdapter.resultList;

                                if (results != null && results.size() > 0) {
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    mAdapter.notifyDataSetInvalidated();
                                }
                            }
                        });
                    }
                }
            };
        }

        edtLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String value = s.toString();

                // Remove all callbacks and messages
                mThreadHandler.removeCallbacksAndMessages(null);

                // Now add a new one
                mThreadHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // Background thread
                        mAdapter.resultList = mAdapter.mCityAPI.autocomplete(value);

                        // Post to Main Thread
                        mThreadHandler.sendEmptyMessage(1);
                    }
                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {
//                doAfterTextChanged();
            }
        });
    }

    @Click(R.id.edtGender)
    void onGenderClicked(View view) {
        openContextMenu(view);
    }

    @Click(R.id.edtDOB)
    void onDOBClicked() {
//        DialogFragment newFragment = new DatePickerFragment();
//        newFragment.show(getSupportFragmentManager(), "datePicker");
        Intent datePickerIntent = new Intent(RegisterActivity.this, DatePickerActivity_.class);
        startActivityForResult(datePickerIntent, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @CheckedChange(R.id.ch_showpwd)
    void onShowPwdClicked(boolean isChecked, CompoundButton button) {
        if (!isChecked) {
            edtPassword.setInputType(129);
        } else {
            edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    @CheckedChange(R.id.ch_conf_showpwd)
    void onConfShowPwdClicked(boolean isChecked, CompoundButton button) {
        if (!isChecked) {
            edtConfirmPassword.setInputType(129);
        } else {
            edtConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    @Click(R.id.txtSubmit)
    void submitClicked() {
        if (!isFromFB) {
            strEmailId = edtEmailIld.getText().toString().trim();
            strPassword = edtPassword.getText().toString().trim();
            strConfirmPassword = edtConfirmPassword.getText().toString().trim();
        }
        String strName = edtName.getText().toString().trim();
        String strLastName = edtLastName.getText().toString().trim();
        String strGender = edtGender.getText().toString().trim();
        String strDOB = edtDOB.getText().toString().trim();
        String strPhoneNumber = edtPhoneNumber.getText().toString().trim();
        String strCollegeName = edtCollegeName.getText().toString().trim();
        String strLocation = edtLocation.getText().toString().trim();


        if (strEmailId.isEmpty()) {
            showToast(getString(R.string.empty_email_id));
        } else if (strPassword.isEmpty()) {
            showToast(getString(R.string.empty_password));
        } else if (strConfirmPassword.isEmpty()) {
            showToast(getString(R.string.empty_confirm_password));
        } else if (strName.isEmpty()) {
            showToast(getString(R.string.empty_name));
        } else if (strLastName.isEmpty()) {
            showToast(getString(R.string.empty_last_name));
        } else if (strGender.isEmpty()) {
            showToast(getString(R.string.empty_gender));
        } else if (strDOB.isEmpty()) {
            showToast(getString(R.string.empty_dob));
        } else if (strPhoneNumber.isEmpty()) {
            showToast(getString(R.string.empty_phone_number));
        } else if (strCollegeName.isEmpty()) {
            showToast(getString(R.string.empty_name));
        } else if (strLocation.isEmpty()) {
            showToast(getString(R.string.empty_location));
        } else if (!strPassword.equals(strConfirmPassword)) {
            showToast(getString(R.string.password_mismatch));
        } else {
            String strBase64 = "";
            if (!isFromFB) {
                strBase64 = Urls.REGISTER_1_GCM_ID + "1234567890" + Urls.REGISTER_2_FIRST_NAME + strName
                        + Urls.REGISTER_3_LAST_NAME + strLastName + Urls.REGISTER_4_PASSWORD + strPassword + Urls.REGISTER_5_EMAIL + strEmailId
                        + Urls.REGISTER_6_LOCATION + strLocation + Urls.REGISTER_7_COLLEGE_NAME + strCollegeName
                        + Urls.REGISTER_8_PHONE_NUMBER + strPhoneNumber + Urls.REGISTER_9_DATE_OF_BIRTH + strDOB
                        + Urls.REGISTER_10_GENDER + strGender;
            } else {
                strBase64 = Urls.FB_REGISTRATION + "1234567890" + Urls.REGISTER_2_FIRST_NAME + strName
                        + Urls.REGISTER_3_LAST_NAME + strLastName + Urls.REGISTER_5_EMAIL + strEmailId +
                        Urls.FB_FBID + strFacebookId + Urls.REGISTER_6_LOCATION + strLocation
                        + Urls.REGISTER_7_COLLEGE_NAME + strCollegeName + Urls.REGISTER_8_PHONE_NUMBER + strPhoneNumber
                        + Urls.REGISTER_9_DATE_OF_BIRTH + strDOB + Urls.REGISTER_10_GENDER + strGender;
            }

            doRegister(strBase64);
        }

    }

    private void doRegister(String param) {
        wsLoadingDialog.showWSLoadingDialog();
        WS_Register wsRegister = new WS_Register(mContext);
        wsRegister.doRegister(param, new WS_Register.RegistrationListener() {
            @Override
            public void onRegistrationDone(ResponseObject responseObject) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (responseObject.getResponseCode() == 500) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("emailid", strEmailId);
                    returnIntent.putExtra("pwd", strPassword);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    JSONObject response = new JSONObject();
                    response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    String error = mCatchJSONExceptions.getStringFromJSON(response, "error");
                    if (error.equals("MESSAGE_EMAIL_EXSITS")) {
                        Toast.makeText(mContext, "Email Already Exists, Please try another", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String sDay, sMonth, sDisplayMonth;
            month = month + 1;
            if (month <= 9) {
                // month = Integer.parseInt("0"+month);
                sMonth = "0" + month;
            } else {
                sMonth = "" + month;
            }
            if (day <= 9) {
                // day = Integer.parseInt("0"+day);
                sDay = "0" + day;
            } else {
                sDay = "" + day;
            }


            sDisplayMonth = getMonth(month);

            strDOB = (sMonth + "/" + sDay + "/" + year);
            edtDOB.setText(strDOB);
        }
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mThreadHandler != null) {
            mThreadHandler.removeCallbacksAndMessages(null);
            mHandlerThread.quit();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        LayoutInflater headerInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.cell_menuheader, null);

        // menu.setHeaderView(header);
        TextView title = (TextView) header.findViewById(R.id.tv_menuheader);

        if (v.getId() == R.id.edtGender) {
            title.setText("Gender");
            menu.setHeaderView(header);
            for (int i = 0; i < genderArray.length; i++) {
                menu.add(1, i, Menu.NONE, genderArray[i]);
                menu.setGroupCheckable(1, true, true);
            }

            if (isGenderSelected) {
                MenuItem menuItem = menu.getItem(genderPosition);
                menuItem.setChecked(true);
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = item.getItemId();
        if (item.getGroupId() == 1) {
            strGender = genderArray[position];
            edtGender.setText(strGender);
            isGenderSelected = true;
            genderPosition = position;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                strDisplayDOB = data.getExtras().getString(Constants.DATE_DISPLAY);
                strDOB = data.getExtras().getString(Constants.DATE_PARAM);
                edtDOB.setText(strDisplayDOB);
            }
        }
    }
}
