package com.smack.questapp;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import utils.Logger;

/**
 * Created by SaraschandraaM on 16/12/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Logger.showInfo("Firebase Token", refreshedToken);
        ApplicationEx.getAppInstance().initiateTokenRegisterListener(refreshedToken);
    }
}
