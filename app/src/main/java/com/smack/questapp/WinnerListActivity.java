package com.smack.questapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapter.WinnerListAdapter;
import model.WinnerObject;
import utils.Constants;

/**
 * Created by SaraschandraaM on 26/10/16.
 */

@EActivity(R.layout.activity_winnerlist)
public class WinnerListActivity extends AppCompatActivity implements WinnerListAdapter.WinnerListClickListener {

    @ViewById(R.id.tl_winnerlist)
    Toolbar mToolbar;

    @ViewById(R.id.rcwinnerlist)
    RecyclerView mRcWinnerList;

    Bundle arguments;
    Context mContext;

    ArrayList<WinnerObject> winnerList;
    WinnerListAdapter winnerListAdapter;

    @AfterViews
    void onViewLoaded() {
        arguments = getIntent().getExtras();
        winnerList = new ArrayList<>();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (arguments != null) {
            winnerList = (ArrayList<WinnerObject>) arguments.getSerializable(Constants.KEY_WINNERLIST);
        }
        winnerListAdapter = new WinnerListAdapter(mContext, winnerList);
        winnerListAdapter.setWinnerClickListener(this);

        mRcWinnerList.setLayoutManager(new LinearLayoutManager(mContext));
        mRcWinnerList.setAdapter(winnerListAdapter);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public void onWinnerListClicked(int position, WinnerObject winnerObject) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
