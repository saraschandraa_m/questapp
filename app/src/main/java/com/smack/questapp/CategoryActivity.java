package com.smack.questapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Random;

import adapter.CoverFlowAdapter;
import aynctask.WS_GetUserRanking;
import aynctask.WS_GetUserResult;
import aynctask.WS_Questions;
import aynctask.WS_WinnerList;
import io.fabric.sdk.android.Fabric;
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;
import listeners.ProfileUpdatedListener;
import listeners.ShareClickListener;
import model.CarouselData;
import model.CategoryItem;
import model.Questions;
import model.UserRanking;
import model.UserResultObject;
import model.WinnerObject;
import utils.Constants;
import utils.SocialMediaShareDialog;
import utils.Tools;
import utils.Urls;
import utils.WSLoadingDialog;


/**
 * Created by SaraschandraaM on 23/06/16.
 */

@EActivity(R.layout.activity_cateogry)
public class CategoryActivity extends AppCompatActivity implements ProfileUpdatedListener {

    @ViewById(R.id.coverflow)
    FeatureCoverFlow mCoverFlow;

    @ViewById(R.id.title)
    TextSwitcher mTitle;

    @ViewById(R.id.tl_category)
    Toolbar mToolbar;

    @ViewById(R.id.drawerlayout)
    DrawerLayout mDrawerLayout;

    @ViewById(R.id.tv_navdrawer_username)
    TextView mTvUserName;

    @ViewById(R.id.iv_navdrawer_profile)
    ImageView mIvProfile;

    ArrayList<CarouselData> photos = new ArrayList<>();
    ArrayList<CategoryItem> categoryItems;
    ArrayList<Questions> sortedQuestionList;
    ArrayList<Integer> addedPositions;

    //    CarouselAdapter adapter;
    ActionBarDrawerToggle mDrawerToogle;

    Context mContext;
    Bundle arguments;
    Random random;
    SharedPreferences mLoginPref;
    SharedPreferences.Editor mLoginEditor;
    WSLoadingDialog wsLoadingDialog;

    String sUserId;
    CoverFlowAdapter mAdapter;
    UserRanking mUserRanking;
    ArrayList<UserResultObject> mUserResultList;

    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        arguments = getIntent().getExtras();
        categoryItems = new ArrayList<>();
        sortedQuestionList = new ArrayList<>();
        addedPositions = new ArrayList<>();
        mUserResultList = new ArrayList<>();
        wsLoadingDialog = new WSLoadingDialog(mContext);
        mLoginPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        mLoginEditor = mLoginPref.edit();
        sUserId = ApplicationEx.getAppInstance().getmUserInfo().getUser_id();


        mDrawerToogle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open, R.string.close);
        mDrawerLayout.setDrawerListener(mDrawerToogle);
        mDrawerToogle.syncState();


        if (arguments != null) {
            categoryItems = (ArrayList<CategoryItem>) arguments.getSerializable(Constants.KEY_CATEGORY);
            if (categoryItems != null && categoryItems.size() > 0) {
                for (int i = 0; i < categoryItems.size(); i++) {
                    photos.add(new CarouselData(categoryItems.get(i).getQuiz_id(), categoryItems.get(i).getQuiz_name()));
                }
            }
        }

        mTitle.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(CategoryActivity.this);
                TextView textView = (TextView) inflater.inflate(R.layout.item_title, null);
                return textView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(this, R.anim.slide_in_top);
        Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        mTitle.setInAnimation(in);
        mTitle.setOutAnimation(out);

        mAdapter = new CoverFlowAdapter(this);
        mAdapter.setData(photos);
        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(mAdapter);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                wsLoadingDialog.showWSLoadingDialog();
                String params = Urls.QUESTIONS + sUserId + Urls.QUIZ_CATEGORY_ID + categoryItems.get(position).getQuiz_id();
                WS_Questions wsQuestions = new WS_Questions(mContext);
                wsQuestions.doGetQuestions(params, new WS_Questions.QuestionsListener() {
                    @Override
                    public void onQuestionsReceived(boolean isSuccess, boolean isQuestionsAvailable, ArrayList<Questions> questionList) {
                        wsLoadingDialog.hideWSLoadingDialog();
                        if (isSuccess) {
                            if (isQuestionsAvailable) {
                                sortedQuestionList = new ArrayList<Questions>();
                                getSortedQuestions(questionList, categoryItems.get(position).getNo_of_questions());
                                if (sortedQuestionList.size() > 0) {
                                    Intent questionsIntent = new Intent(CategoryActivity.this, QuestionsActivity_.class);
                                    questionsIntent.putExtra(Constants.KEY_DURATION, categoryItems.get(position).getDuration());
                                    questionsIntent.putExtra(Constants.KEY_QUESTION, sortedQuestionList);
                                    questionsIntent.putExtra(Constants.KEY_CATEGORY_NAME, categoryItems.get(position).getQuiz_name());
                                    questionsIntent.putExtra(Constants.KEY_CATEGORY_ID, categoryItems.get(position).getQuiz_id());
                                    startActivity(questionsIntent);
                                }
                            } else {
                                Toast.makeText(mContext, "No Questions Under this Category", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                mTitle.setText(photos.get(position).titleResId);
            }

            @Override
            public void onScrolling() {
                mTitle.setText("");
            }
        });

        ApplicationEx.getAppInstance().setProfileUpdatedListener(this);
        setUserProfileDetail();

    }

    @Click(R.id.ll_nav_signout)
    void onSignoutClicked() {
        mLoginEditor.putString(Constants.PREF_USERNAME, "");
        mLoginEditor.putBoolean(Constants.PREF_ISLOGGED, false);
        mLoginEditor.putBoolean(Constants.KEY_FROMFB, false);
        mLoginEditor.putString(Constants.PREF_USERDETAILS, "");
        mLoginEditor.commit();
        Intent signoutIntent = new Intent(CategoryActivity.this, SplashActivity_.class);
        startActivity(signoutIntent);
        finish();
    }

    @Click(R.id.rl_userprofile)
    void onUserProfile() {
        getUserRanking();
    }

    @Click(R.id.rl_winnerlist)
    void onWinnerListClicked() {
        getWinnerList();
    }

    @Click(R.id.iv_navdrawer_profile)
    void onProfileImageClicked() {
        if (Tools.checkIfStringisValid(ApplicationEx.getAppInstance().getmUserInfo().getProfile_image())) {
            Intent imageViewIntent = new Intent(CategoryActivity.this, ViewImageActivity_.class);
            imageViewIntent.putExtra(Constants.KEY_IMAGEURL, ApplicationEx.getAppInstance().getmUserInfo().getProfile_image());
            startActivity(imageViewIntent);
        }
    }

    @Click(R.id.rl_share)
    void onShareClicked() {
        new SocialMediaShareDialog(mContext).showDialog(new ShareClickListener() {
            @Override
            public void onShareClicked(int shareCode) {
                if (shareCode == 0) {
                    shareContentToFacebook();
                } else if (shareCode == 1) {
                    shareContentToTwitter();
                }
            }
        });
    }

    private void getSortedQuestions(ArrayList<Questions> questions, int noofQuestions) {
        random = new Random();
        for (int i = 0; i < questions.size(); i++) {
            Questions sortedQuestion = questions.get(random.nextInt(questions.size()));
            if (!sortedQuestionList.contains(sortedQuestion)) {
                if (sortedQuestionList.size() < noofQuestions) {
                    sortedQuestionList.add(sortedQuestion);
                }
            }
        }
        if (sortedQuestionList.size() < noofQuestions) {
            getSortedQuestions(questions, noofQuestions);
        }
    }

    private void getWinnerList() {
        String param = Urls.GET_WINNER_LIST + sUserId;
        new WS_WinnerList(mContext, param).execute(new WS_WinnerList.WinnerListReceivedListener() {
            @Override
            public void onWinnerListReceived(boolean isSuccess, ArrayList<WinnerObject> winnerList) {
                if (isSuccess) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    Intent winnerListIntent = new Intent(CategoryActivity.this, WinnerListActivity_.class);
                    winnerListIntent.putExtra(Constants.KEY_WINNERLIST, winnerList);
                    startActivity(winnerListIntent);
                } else {
                    Snackbar.make(mDrawerLayout, "No Winner List Available", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getUserRanking() {
        String param = Urls.GET_USER_RANKING + sUserId;
        wsLoadingDialog.showWSLoadingDialog();
        new WS_GetUserRanking(mContext, param).execute(new WS_GetUserRanking.UserRankingReceivedListener() {
            @Override
            public void onUserRankingReceived(boolean isSuccess, UserRanking userRanking) {
                if (isSuccess) {
                    mUserRanking = userRanking;
                    getUserResult();
                } else {
                    wsLoadingDialog.hideWSLoadingDialog();
                }
            }
        });
    }

    private void getUserResult() {
        String param = Urls.GET_USER_RESULTS + sUserId;
        new WS_GetUserResult(mContext, param).execute(new WS_GetUserResult.UserResultListener() {
            @Override
            public void onUserResultReceived(boolean isSuccess, ArrayList<UserResultObject> userResultList) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (isSuccess) {
                    mUserResultList = userResultList;
                } else {
                    mUserResultList = new ArrayList<UserResultObject>();
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                moveToMainProfileActivity();
            }
        });
    }

    private void moveToMainProfileActivity() {
        Intent profileIntent = new Intent(CategoryActivity.this, MainProfileActivity_.class);
        profileIntent.putExtra(Constants.KEY_USERRANKING, mUserRanking);
        profileIntent.putExtra(Constants.KEY_USERRESULTLIST, mUserResultList);
        startActivity(profileIntent);
    }

    private void setUserProfileDetail() {
        mTvUserName.setText(ApplicationEx.getAppInstance().getmUserInfo().getFirst_name());
        if (Tools.checkIfStringisValid(ApplicationEx.getAppInstance().getmUserInfo().getProfile_image())) {
            Picasso.with(mContext).load(ApplicationEx.getAppInstance().getmUserInfo().getProfile_image()).placeholder(R.drawable.img_profile).into(mIvProfile);
        }
    }

    private void shareContentToFacebook() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://bfxe9.app.goo.gl/V9Hh"))
                .setContentTitle("Quest App")
                .setContentDescription("Take a Online quiz and get recharge for your mobile when won.")
                .build();

        ShareDialog shareDialog = new ShareDialog(this);
        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
    }

    private void shareContentToTwitter() {
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("Hi Just Took a Online quiz using the Quest App. https://bfxe9.app.goo.gl/V9Hh");
        builder.show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onProfileUpdated() {
        setUserProfileDetail();
    }
}
