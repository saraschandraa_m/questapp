package com.smack.questapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapter.PlacesAutoCompleteAdapter;
import aynctask.WS_UpdateUserProfile;
import aynctask.WS_UploadImage;
import model.UserInfo;
import utils.Constants;
import utils.Tools;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 29/10/16.
 */

@EActivity(R.layout.activity_editprofile)
public class EditProfileActivity extends AppCompatActivity {


    @ViewById(R.id.tl_profileedit)
    Toolbar mToolbar;

    @ViewById(R.id.et_editprof_firstname)
    EditText mEtFirstName;

    @ViewById(R.id.et_editprof_lastname)
    EditText mEtLastName;

    @ViewById(R.id.et_editprof_dob)
    EditText mEtDOB;

    @ViewById(R.id.et_editprof_collegename)
    EditText mEtCollege;

    @ViewById(R.id.et_editprof_phoneno)
    EditText mEtPhoneno;

    @ViewById(R.id.iv_edit_profileimage)
    ImageView mIvProfileImage;

    @ViewById(R.id.et_editprof_location)
    AutoCompleteTextView mEtLocation;

    Context mContext;
    Bundle arguments;
    UserInfo userDetail, updateUserDetail;
    String strDisplayDOB, strDOB, strFilePath;
    String[] strDateArr = new String[3];
    public static final int REQUEST_READGALLERY = 0;
    Bitmap mSelectedImageBitmap;
    boolean isImageChanged = false;
    WSLoadingDialog wsLoadingDialog;

    Handler mThreadHandler;
    HandlerThread mHandlerThread;

    PlacesAutoCompleteAdapter mAdapter;


    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        userDetail = ApplicationEx.getAppInstance().getmUserInfo();
        updateUserDetail = new UserInfo();
        if (userDetail != null) {
            mEtFirstName.setText(userDetail.getFirst_name());
            mEtLastName.setText(userDetail.getLast_name());
            mEtCollege.setText(userDetail.getCollege_name());
            strDOB = userDetail.getDob();
            strDateArr = Tools.getFormattedProfileDate(userDetail.getDob());
            strDisplayDOB = strDateArr[0] + " " + strDateArr[1] + " " + strDateArr[2];
            mEtDOB.setText(strDisplayDOB);
            mEtLocation.setText(userDetail.getLocation());
            mEtPhoneno.setText(userDetail.getContact_no());
            if (Tools.checkIfStringisValid(userDetail.getProfile_image())) {
                Picasso.with(mContext).load(userDetail.getProfile_image()).placeholder(R.drawable.img_profile).into(mIvProfileImage);
            }
        }
        wsLoadingDialog = new WSLoadingDialog(mContext);

        mAdapter = new PlacesAutoCompleteAdapter(mContext, R.layout.cell_auto_complete);
        mEtLocation.setImeActionLabel(getResources().getString(R.string.accept), EditorInfo.IME_ACTION_DONE);
        mEtLocation.setAdapter(mAdapter);

        if (mThreadHandler == null) {
            // Initialize and start the HandlerThread
            // which is basically a Thread with a Looper
            // attached (hence a MessageQueue)
            mHandlerThread = new HandlerThread("REGISTER", android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mHandlerThread.start();

            // Initialize the Handler
            mThreadHandler = new Handler(mHandlerThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<String> results = mAdapter.resultList;

                                if (results != null && results.size() > 0) {
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    mAdapter.notifyDataSetInvalidated();
                                }
                            }
                        });
                    }
                }
            };
        }

        mEtLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String value = s.toString();

                // Remove all callbacks and messages
                mThreadHandler.removeCallbacksAndMessages(null);

                // Now add a new one
                mThreadHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // Background thread
                        mAdapter.resultList = mAdapter.mCityAPI.autocomplete(value);

                        // Post to Main Thread
                        mThreadHandler.sendEmptyMessage(1);
                    }
                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {
//                doAfterTextChanged();
            }
        });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editprofile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_editprof_done:
                wsLoadingDialog.showWSLoadingDialog();
                if (!isImageChanged) {
                    getProfileDetails();
                } else {
                    uploadToServer(strFilePath);
                }
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    strDisplayDOB = data.getExtras().getString(Constants.DATE_DISPLAY);
                    strDOB = data.getExtras().getString(Constants.DATE_PARAM);
                    mEtDOB.setText(strDisplayDOB);
                }
                break;

            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    byte[] byteArray = data.getByteArrayExtra("selectedPhoto");
                    strFilePath = data.getExtras().getString("selectedPath");
                    mSelectedImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    mIvProfileImage.setImageBitmap(mSelectedImageBitmap);
                    isImageChanged = true;
                }
                break;
        }

    }

    private void loadPermissions(String readPhonestatePermission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, readPhonestatePermission) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, readPhonestatePermission)) {
                ActivityCompat.requestPermissions(this, new String[]{readPhonestatePermission}, requestCode);
            }
        } else {
            moveToGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READGALLERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {                      //granted
                    moveToGallery();
                } else {
                }

                return;
            }
        }
    }

    @Click(R.id.et_editprof_dob)
    void onDOBClicked() {
        Intent datePickerIntent = new Intent(EditProfileActivity.this, DatePickerActivity_.class);
        startActivityForResult(datePickerIntent, 0);
    }

    @Click(R.id.iv_edit_profileimage)
    void onEditProfileImage() {
        if (Build.VERSION.SDK_INT < 23) {
            moveToGallery();
        } else {
            loadPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_READGALLERY);
        }
    }

    private void getProfileDetails() {
        updateUserDetail.setUser_id(userDetail.getUser_id());
        updateUserDetail.setFirst_name(mEtFirstName.getText().toString());
        updateUserDetail.setLast_name(mEtLastName.getText().toString());
        updateUserDetail.setCollege_name(mEtCollege.getText().toString());
        updateUserDetail.setDob(strDOB);
        updateUserDetail.setGender(userDetail.getGender());
        updateUserDetail.setContact_no(mEtPhoneno.getText().toString());
        updateUserDetail.setLocation(mEtLocation.getText().toString());

        new WS_UpdateUserProfile(mContext, updateUserDetail).execute(new WS_UpdateUserProfile.ProfileUpdateListener() {
            @Override
            public void onProfileUpdated(boolean isSuccess, UserInfo userInfo) {
                if (isSuccess) {
                    wsLoadingDialog.hideWSLoadingDialog();
                    ApplicationEx.getAppInstance().initiateProfileUpdatedListener();
                    Intent updatedProfile = new Intent();
                    updatedProfile.putExtra(Constants.KEY_USERINFO, userInfo);
                    setResult(Activity.RESULT_OK, updatedProfile);
                    finish();
                }
            }
        });
    }

    private void moveToGallery() {
        Intent coverPhotoIntent = new Intent(EditProfileActivity.this, PhotoGalleryActivity_.class);
        coverPhotoIntent.putExtra("screenType", 0);
        startActivityForResult(coverPhotoIntent, 1);
    }

    private void uploadToServer(String path) {
        new WS_UploadImage(mContext).doUploadImage(path,
                userDetail.getUser_id(), new WS_UploadImage.UploadImageListener() {
                    @Override
                    public void onImageUploading() {

                    }

                    @Override
                    public void onImageUploaded(boolean isSuccess, String ImageURL) {
                        Log.i("ImageURL", ImageURL);
                        if (isSuccess) {
                            getProfileDetails();
                        }
                    }
                });
    }
}
