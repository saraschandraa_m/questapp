package com.smack.questapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import model.UserInfo;
import utils.Tools;

/**
 * Created by SaraschandraaM on 07/11/16.
 */

@EActivity(R.layout.activity_profileview)
public class ProfileViewActivity extends AppCompatActivity {

    @ViewById(R.id.tv_profview_name)
    TextView mTvName;

    @ViewById(R.id.tv_profview_college)
    TextView mTvCollege;

    @ViewById(R.id.tv_profview_dob)
    TextView mTvDOB;

    @ViewById(R.id.tv_profview_location)
    TextView mTvLocation;

    UserInfo userInfo;
    String[] displayDate = new String[3];

    @AfterViews
    void onViewLoaded() {
        userInfo = ApplicationEx.getAppInstance().getmUserInfo();

        if (userInfo != null) {
            mTvName.setText(userInfo.getFirst_name() + " " + userInfo.getLast_name());
            mTvCollege.setText(userInfo.getCollege_name());
            displayDate = Tools.getFormattedProfileDate(userInfo.getDob());
            mTvDOB.setText(displayDate[0] + " " + displayDate[1] + " " + displayDate[2]);
            mTvLocation.setText(userInfo.getLocation());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Click(R.id.vw_left)
    void onLeftContentClicked() {
        finish();
    }

    @Click(R.id.vw_right)
    void onRightContentClicked() {
        finish();
    }

    @Click(R.id.ll_profviewcontent)
    void onContentAreaClicked() {
        finish();
    }
}
