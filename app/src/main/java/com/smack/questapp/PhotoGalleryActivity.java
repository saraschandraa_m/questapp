package com.smack.questapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import adapter.ImageGalleryAdapter;

/**
 * Created by SaraschandraaM on 09/03/16.
 */
@EActivity(R.layout.activity_photogallery)
public class PhotoGalleryActivity extends AppCompatActivity {


    @ViewById(R.id.photoGalleryGridview)
    GridView mPhotoGalleryGrid;

    @ViewById(R.id.rcphotogallery)
    RecyclerView mRcPhotoGallery;

    @ViewById(R.id.tl_gallery)
    Toolbar mTlGallery;

    ImageGalleryAdapter mImageGalleryAdapter;
    Context mContext;
    LoadDataAsynTask loadDataAsynTask;

    List<String> photoPaths;

    public static ArrayList<String> selectedPath;
    int selectionType;
    Bundle args;

    @AfterViews
    void onViewLoaded() {
        mContext = PhotoGalleryActivity.this;
        setSupportActionBar(mTlGallery);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        photoPaths = new ArrayList<>();
        selectedPath = new ArrayList<>();
        args = getIntent().getExtras();
        if (args != null) {
            selectionType = args.getInt("screenType", 0);
        }
        mRcPhotoGallery.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));


        loadDataAsynTask = new LoadDataAsynTask();
        loadDataAsynTask.execute();
    }

    private void onDoneClicked() {
        if (selectedPath.size() > 0) {
            switch (selectionType) {
                case 0:
                    Intent cropIntent = new Intent(PhotoGalleryActivity.this, CropEventCoverActivity_.class);
                    cropIntent.putStringArrayListExtra("selectedPhotos", selectedPath);
                    ApplicationEx.getAppInstance().selectedPhotoPath = selectedPath.get(0);
                    startActivityForResult(cropIntent, 0);
                    break;

                case 1:
                    Intent eventCoverIntent = new Intent(PhotoGalleryActivity.this, CropEventCoverActivity_.class);
                    eventCoverIntent.putStringArrayListExtra("selectedPhotos", selectedPath);
                    ApplicationEx.getAppInstance().selectedPhotoPath = selectedPath.get(0);
                    startActivityForResult(eventCoverIntent, 1);
                    break;

                case 2:
                    Intent eventAlbumIntent = new Intent(PhotoGalleryActivity.this, CropEventCoverActivity_.class);
                    eventAlbumIntent.putStringArrayListExtra("selectedPhotos", selectedPath);
                    ApplicationEx.getAppInstance().selectedPhotoPath = selectedPath.get(0);
                    startActivityForResult(eventAlbumIntent, 2);
                    break;
            }
        } else {
            Toast.makeText(mContext, "Please select a image", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private class LoadDataAsynTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // HomeFragmentActivity.defaultInstance().showProgressBar();
            // getActivity().sendBroadcast(
            // new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri
            // .fromFile(Environment.getExternalStorageDirectory())));
        }

        @Override
        protected Void doInBackground(String... params) {

            @SuppressWarnings("deprecation")
            Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, params, null, null, null);
            try {
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                            if (path.contains("DCIM/")) {
                                photoPaths.add(path);
                            }
                        } while (cursor.moveToNext());

                    }
                }
            } catch (Exception e) {
            }
            mImageGalleryAdapter = new ImageGalleryAdapter(mContext, photoPaths, 0);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mRcPhotoGallery.setAdapter(mImageGalleryAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_gallery_done:
                onDoneClicked();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                byte[] profielByteArray = data.getByteArrayExtra("selectedPhoto");
                String filepath = data.getExtras().getString("selectedPath");
                Bitmap selectedProfile = BitmapFactory.decodeByteArray(profielByteArray, 0, profielByteArray.length);
                Intent profileIntent = new Intent();
                profileIntent.putExtra("selectedPhoto", profielByteArray);
                profileIntent.putExtra("selectedPath", filepath);
                setResult(Activity.RESULT_OK, profileIntent);
                finish();
                break;

            case 1:
                byte[] eventByteArray = data.getByteArrayExtra("selectedPhoto");
                String eventpath = data.getExtras().getString("selectedPath");
                Bitmap selectedPhoto = BitmapFactory.decodeByteArray(eventByteArray, 0, eventByteArray.length);
                Intent eventIntent = new Intent();
                eventIntent.putExtra("selectedPhoto", eventByteArray);
                eventIntent.putExtra("selectedPath", eventpath);
                setResult(Activity.RESULT_OK, eventIntent);
                finish();
                break;

            case 2:
                byte[] eventAlbumByteArray = data.getByteArrayExtra("selectedPhoto");
                String albumpath = data.getExtras().getString("selectedPath");
                Bitmap albumPhoto = BitmapFactory.decodeByteArray(eventAlbumByteArray, 0, eventAlbumByteArray.length);
                Intent eventAlbumIntent = new Intent();
                eventAlbumIntent.putExtra("selectedPhoto", eventAlbumByteArray);
                eventAlbumIntent.putExtra("selectedPath", albumpath);
                setResult(Activity.RESULT_OK, eventAlbumIntent);
                finish();
                break;
        }
    }
}
