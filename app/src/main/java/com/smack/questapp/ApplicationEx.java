package com.smack.questapp;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import listeners.ProfileUpdatedListener;
import listeners.TokenRegisterListener;
import model.UserInfo;

/**
 * Created by SaraschandraaM on 22/06/16.
 */
public class ApplicationEx extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "saraschandraa.naidu@hotmail.com";
    private static final String TWITTER_SECRET = "bep0s1t1ve";



    public static ApplicationEx appInstance;

    public static ApplicationEx getAppInstance() {
        return appInstance;
    }

    public String sUserId, sGCMRegID;
    public UserInfo mUserInfo;

    public String selectedPhotoPath;

    ProfileUpdatedListener profileUpdatedListener;
    TokenRegisterListener tokenRegisterListener;

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        appInstance = this;
    }

    public static Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(path), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 1000;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 4;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap image = BitmapFactory.decodeStream(new FileInputStream(path), null, o2);

            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(image, image.getWidth(), image.getHeight(), true);

            Bitmap bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            return bitmap;
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public String getsUserId() {
        return sUserId;
    }

    public void setsUserId(String sUserId) {
        this.sUserId = sUserId;
    }

    public UserInfo getmUserInfo() {
        return mUserInfo;
    }

    public void setmUserInfo(UserInfo mUserInfo) {
        this.mUserInfo = mUserInfo;
    }

    public String getsGCMRegID() {
        return sGCMRegID;
    }

    public void setsGCMRegID(String sGCMRegID) {
        this.sGCMRegID = sGCMRegID;
    }

    public void setProfileUpdatedListener(ProfileUpdatedListener profileUpdatedListener) {
        this.profileUpdatedListener = profileUpdatedListener;
    }

    public void initiateProfileUpdatedListener() {
        if (profileUpdatedListener != null) {
            profileUpdatedListener.onProfileUpdated();
        }
    }

    public void setTokenRegisterListener(TokenRegisterListener tokenRegisterListener) {
        this.tokenRegisterListener = tokenRegisterListener;
    }

    public void initiateTokenRegisterListener(String strRegId) {
        if (tokenRegisterListener != null) {
            tokenRegisterListener.onTokenRegistered(strRegId);
        }
    }
}
