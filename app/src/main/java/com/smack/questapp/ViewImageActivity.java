package com.smack.questapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import utils.Constants;

/**
 * Created by SaraschandraaM on 19/09/16.
 */
@EActivity(R.layout.activity_viewimage)
public class ViewImageActivity extends AppCompatActivity {

    @ViewById(R.id.iv_viewimage)
    ImageView mIvImage;

    Context mContext;
    Bundle arguments;

    String imgURL;

    @AfterViews
    void onViewLoaded() {
        arguments = getIntent().getExtras();
        if (arguments != null) {
            imgURL = arguments.getString(Constants.KEY_IMAGEURL);
        }
        Picasso.with(mContext).load(imgURL).into(mIvImage);
    }

    @Click(R.id.rl_closeview)
    void onCloseViewClicked() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mContext = this;
    }
}
