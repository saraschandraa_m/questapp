package com.smack.questapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import aynctask.WS_Category;
import aynctask.WS_UpdateUserProfile;
import aynctask.WS_UploadImage;
import model.CategoryItem;
import model.UserInfo;
import utils.Constants;

/**
 * Created by SaraschandraaM on 23/11/16.
 */

@EActivity(R.layout.activity_profileimage)
public class ProfileImageActivity extends AppCompatActivity {

    @ViewById(R.id.tl_profileimage)
    Toolbar mToolbar;

    @ViewById(R.id.iv_profileimg)
    ImageView mIvProfile;

    Bundle arguments;
    Context mContext;
    SharedPreferences ImagePref;
    SharedPreferences.Editor ImagePrefEditor;

    UserInfo mUserDetail;
    String sUserID, strFilePath;
    boolean isSkipPressed, isImageChanged = false;
    public static final int REQUEST_READGALLERY = 0;
    Bitmap mSelectedImageBitmap;

    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        arguments = getIntent().getExtras();
        ImagePref = PreferenceManager.getDefaultSharedPreferences(mContext);
        ImagePrefEditor = ImagePref.edit();

        if (arguments != null) {
            sUserID = arguments.getString(Constants.KEY_USERID);
            mUserDetail = ApplicationEx.getAppInstance().getmUserInfo();
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profileimg, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_skip:
                doGetCategory(sUserID);
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    byte[] byteArray = data.getByteArrayExtra("selectedPhoto");
                    strFilePath = data.getExtras().getString("selectedPath");
                    mSelectedImageBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    mIvProfile.setImageBitmap(mSelectedImageBitmap);
                    isImageChanged = true;
                }
                break;
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READGALLERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {                      //granted
                    moveToGallery();
                } else {
                }

                return;
            }
        }
    }


    @Click(R.id.iv_profileimg)
    void onProfileImageClicked() {
        if (Build.VERSION.SDK_INT < 23) {
            moveToGallery();
        } else {
            loadPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_READGALLERY);
        }
    }

    @Click(R.id.tv_upload_profileimg)
    void onUploadProfileImageClicked() {
        uploadToServer(strFilePath);
    }

    private void loadPermissions(String readPhonestatePermission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, readPhonestatePermission) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, readPhonestatePermission)) {
                ActivityCompat.requestPermissions(this, new String[]{readPhonestatePermission}, requestCode);
            }
        } else {
            moveToGallery();
        }
    }

    private void moveToGallery() {
        Intent coverPhotoIntent = new Intent(ProfileImageActivity.this, PhotoGalleryActivity_.class);
        coverPhotoIntent.putExtra("screenType", 0);
        startActivityForResult(coverPhotoIntent, 1);
    }

    private void uploadToServer(String path) {
        new WS_UploadImage(mContext).doUploadImage(path,
                mUserDetail.getUser_id(), new WS_UploadImage.UploadImageListener() {
                    @Override
                    public void onImageUploading() {

                    }

                    @Override
                    public void onImageUploaded(boolean isSuccess, String ImageURL) {
                        Log.i("ImageURL", ImageURL);
                        if (isSuccess) {
                            getProfileDetails();
                        }
                    }
                });
    }

    private void getProfileDetails() {

        new WS_UpdateUserProfile(mContext, mUserDetail).execute(new WS_UpdateUserProfile.ProfileUpdateListener() {
            @Override
            public void onProfileUpdated(boolean isSuccess, UserInfo userInfo) {
                if (isSuccess) {
                    ApplicationEx.getAppInstance().setmUserInfo(userInfo);
                    doGetCategory(userInfo.getUser_id());
                }
            }
        });
    }

    private void doGetCategory(String userID) {
        WS_Category wsCategory = new WS_Category(mContext);
        wsCategory.doGetCategory(userID, new WS_Category.CategoryListener() {
            @Override
            public void onCategoryReceived(boolean isSuccess, ArrayList<CategoryItem> categoryList) {
                if (isSuccess) {
                    Intent loginIntent = new Intent(ProfileImageActivity.this, CategoryActivity_.class);
                    loginIntent.putExtra(Constants.KEY_CATEGORY, categoryList);
                    startActivity(loginIntent);
                    finish();
                }
            }
        });
    }
}
