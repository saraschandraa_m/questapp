package com.smack.questapp;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import model.Questions;
import utils.Constants;
import utils.Tools;

/**
 * Created by SaraschandraaM on 31/07/16.
 */
@EFragment(R.layout.fragment_questions)
public class QuestionsFragment extends Fragment {

    @ViewById(R.id.tv_que_question)
    TextView mTvQuestion;

    @ViewById(R.id.tv_done)
    TextView mTvDone;

    @ViewById(R.id.tv_que_quelist)
    TextView mTvQuestionList;

    @ViewById(R.id.tv_que_result)
    TextView mTvQuestionResult;

    @ViewById(R.id.rg_que_options)
    RadioGroup mRgOptions;

    @ViewById(R.id.root_questionsfrag)
    RelativeLayout mRootLayout;

    @ViewById(R.id.iv_que_image)
    ImageView mIvImageQuestion;

    Context mContext;
    Bundle arguments;

    int position;
    ArrayList<Questions> questionList;
    Questions currentQuestion;
    String selectedOption = "", URL = "http://quiz.thedevelopersolutions.com";
    boolean isSkipAlertShown;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        position = arguments.getInt(Constants.KEY_POSITION, 0);
        questionList = QuestionsActivity.getRootInstance().questionList;
        currentQuestion = questionList.get(position);
        isSkipAlertShown = false;
        if (position < questionList.size() - 1) {
            mTvDone.setText("Next");
        } else {
            mTvDone.setText("Done");
        }
        mTvQuestion.setText(currentQuestion.getQuestion());
        mTvQuestionList.setText("" + (position + 1) + "/" + questionList.size());
        if (Tools.checkIfURLisValid(currentQuestion.getQuestion_image())) {
            mIvImageQuestion.setVisibility(View.VISIBLE);
            URL = URL + currentQuestion.getQuestion_image();
            Picasso.with(mContext).load(URL).into(mIvImageQuestion);
        } else {
            mIvImageQuestion.setVisibility(View.GONE);
        }

        RadioGroup.LayoutParams params_rb = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params_rb.setMargins(10, 10, 10, 10);
        for (int i = 0; i < currentQuestion.getQuestionsOptionses().size(); i++) {
            AppCompatRadioButton radioButton = new AppCompatRadioButton(getActivity());
            radioButton.setTextColor(getResources().getColor(R.color.color_white));
            radioButton.setHighlightColor(getResources().getColor(R.color.color_white));
            radioButton.setText(currentQuestion.getQuestionsOptionses().get(i).getQuestion_option());
            radioButton.setId(i);
            mRgOptions.addView(radioButton, params_rb);
        }

        mRgOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int checkedRadioButtonId = mRgOptions.getCheckedRadioButtonId();
                RadioButton radioBtn = (RadioButton) getActivity().findViewById(checkedRadioButtonId);
                selectedOption = radioBtn.getText().toString();
            }
        });

    }

    @Click(R.id.tv_done)
    void onNextClicked() {
        if (!selectedOption.isEmpty()) {
            String message = "";
            int length = 1000;
            if (selectedOption.equals(currentQuestion.getAnswer())) {
                QuestionsActivity.getRootInstance().noCorrectQuestions++;
                message = "Correct!";
//                mTvQuestionResult.setText("Correct!");
            } else {
                message = "Wrong, The correct answer is " + currentQuestion.getAnswer();
            }
            Snackbar.make(mRootLayout, message, Snackbar.LENGTH_SHORT).show();
            Handler nextHandler = new Handler();
            nextHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    moveNext();
                }
            }, 1000);
        } else {
            if (!isSkipAlertShown) {
                Toast.makeText(mContext, "Please select a option or press next again to skip this question", Toast.LENGTH_SHORT).show();
                isSkipAlertShown = true;
            } else {
                QuestionsActivity.getRootInstance().changeQuestionPosition(position + 1);
            }
        }
    }

    private void moveNext() {
        if (mTvDone.getText().equals("Next")) {
            QuestionsActivity.getRootInstance().changeQuestionPosition(position + 1);
        } else if (mTvDone.getText().equals("Done")) {
            QuestionsActivity.getRootInstance().changeToResult(false);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
