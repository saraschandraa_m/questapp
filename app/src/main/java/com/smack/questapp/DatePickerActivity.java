package com.smack.questapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

import utils.Constants;
import utils.Tools;

/**
 * Created by SaraschandraaM on 27/09/16.
 */


@EActivity(R.layout.activity_datepicker)
public class DatePickerActivity extends AppCompatActivity {

    @ViewById(R.id.dt_picker)
    DatePicker mDatePicker;

    @ViewById(R.id.time_picker)
    TimePicker mTimePicker;

    @ViewById(R.id.ll_datepicker)
    LinearLayout mLlDatePicker;

    @ViewById(R.id.ll_timepicker)
    LinearLayout mLlTimePicker;

    Context mContext;
    Bundle arguments;

    boolean isFromProfile;
    String stDate, stTime, stDisplayDateTime, stParamDateTime;

    @AfterViews
    void onViewLoaded() {
        arguments = getIntent().getExtras();
    }

    @Click(R.id.tv_btn_selectdate)
    void onDateSelected() {
        int day, month, year;
        day = mDatePicker.getDayOfMonth();
        month = mDatePicker.getMonth();
        year = mDatePicker.getYear();
        String sDay, sMonth, sDisplayMonth;
        month = month + 1;
        if (month <= 9) {
            sMonth = "0" + month;                                                                   // month = Integer.parseInt("0"+month);
        } else {
            sMonth = "" + month;
        }
        if (day <= 9) {
            sDay = "0" + day;                                                                       // day = Integer.parseInt("0"+day);
        } else {
            sDay = "" + day;
        }
        sDisplayMonth = Tools.getMonth(sMonth + "/" + sDay + "/" + year);
        stDate = sDay + " " + sDisplayMonth + " " + year;
        stDisplayDateTime = sDay + " " + sDisplayMonth + " " + year;
        stParamDateTime = year + "-" + sMonth + "-" + sDay;
        moveToPreviousScreen();

    }

    @Click(R.id.tv_btn_selecttime)
    void onTimeSelected() {
        int hour, minute, second;
        String min;
        int BaseLine = mTimePicker.getBaseline();
        hour = mTimePicker.getCurrentHour();
        minute = mTimePicker.getCurrentMinute();
        if (minute < 10) {
            min = "" + 0 + minute;
        } else {
            min = "" + minute;
        }
//        if (second < 10) {
//            sec = "" + 0 + second;
//        } else {
//            sec = "" + second;
//        }
        stTime = "" + hour + ":" + min;

        stParamDateTime = stParamDateTime + " " + hour + ":" + min;
        stDisplayDateTime = stDisplayDateTime + " " + Tools.get12HourFormat(hour, minute);
        Log.i("Display Time", stDisplayDateTime);
        Log.i("Param Time", stParamDateTime);

        moveToPreviousScreen();
    }

    @Click(R.id.tv_btn_selectdate_cancel)
    void onCancelDateClicked() {
        finish();
    }

    public String getMonth(int month) {
        String stMonth = "" + month;
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(stMonth);
        return month_name;
    }


    private void moveToPreviousScreen() {
        Intent datetimeIntent = new Intent();
        datetimeIntent.putExtra(Constants.DATE_DISPLAY, stDisplayDateTime);
        datetimeIntent.putExtra(Constants.DATE_PARAM, stParamDateTime);
        setResult(Activity.RESULT_OK, datetimeIntent);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }
}
