package com.smack.questapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapter.CompleteResultAdapter;
import model.UserResultObject;
import utils.Constants;

/**
 * Created by SaraschandraaM on 09/11/16.
 */

@EActivity(R.layout.activity_completelist)
public class CompleteListActivity extends AppCompatActivity implements CompleteResultAdapter.ResultDetailClickListener {

    @ViewById(R.id.tl_completelist)
    Toolbar mToolbar;

    @ViewById(R.id.rccompletelist)
    RecyclerView mRcCompleteList;

    Bundle args;
    Context mContext;
    ArrayList<UserResultObject> mUserResultList;

    CompleteResultAdapter completeResultAdapter;


    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        args = getIntent().getExtras();
        if (args != null) {
            mUserResultList = (ArrayList<UserResultObject>) args.getSerializable(Constants.KEY_USERRESULTLIST);
        }
        mRcCompleteList.setLayoutManager(new LinearLayoutManager(mContext));

        completeResultAdapter = new CompleteResultAdapter(mContext, mUserResultList);
        completeResultAdapter.setResultDetailClickListener(this);
        mRcCompleteList.setAdapter(completeResultAdapter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onResultDetailClicked(UserResultObject userResultObject) {
        moveToResultDetailScreen(userResultObject);
    }

    private void moveToResultDetailScreen(UserResultObject userResultObject) {
        Intent resultDetailIntent = new Intent(CompleteListActivity.this, ResultDetailActivity_.class);
        resultDetailIntent.putExtra(Constants.KEY_USERRESULTOBJ, userResultObject);
        startActivity(resultDetailIntent);
    }
}
