package com.smack.questapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import utils.Constants;

/**
 * Created by SaraschandraaM on 01/08/16.
 */
@EFragment(R.layout.fragment_result)
public class ResultFragment extends Fragment {

    @ViewById(R.id.tv_quizresult)
    TextView mTvQuizResult;

    Context mContext;
    Bundle arguments;

    int result, totalNoofQuest;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        result = arguments.getInt(Constants.KEY_RESULT, 0);
        totalNoofQuest = arguments.getInt(Constants.KEY_NOQUESTION, 0);
        mTvQuizResult.setText("You have answered " + result + " out of " + totalNoofQuest);
    }

    @Click(R.id.ll_result_submit)
    void onSubmitResultClicked() {
        QuestionsActivity.getRootInstance().finish();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
