package com.smack.questapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import model.UserInfo;
import model.UserRanking;
import model.UserResultObject;
import utils.Constants;
import utils.Tools;

/**
 * Created by SaraschandraaM on 22/10/16.
 */

@EActivity(R.layout.activity_profile)
public class MainProfileActivity extends AppCompatActivity {

    @ViewById(R.id.tl_mainprofile)
    Toolbar mToolbar;

    @ViewById(R.id.tv_ranking_noofquiz)
    TextView mTvNoOfQuiz;

    @ViewById(R.id.tv_ranking_personalrating)
    TextView mTvPersonalRating;

    @ViewById(R.id.tv_ranking_win)
    TextView mTvWinPercentage;

    @ViewById(R.id.tv_ranking_nooftimeswon)
    TextView mTvNoofTimeWon;

    @ViewById(R.id.tv_ranking_username)
    TextView mTvUserName;

    @ViewById(R.id.iv_profilepic)
    ImageView mIvProfilePic;

    @ViewById(R.id.root_mainprofile)
    LinearLayout mRootLayout;

    Bundle arguments;
    Context mContext;

    UserRanking mUserRanking;
    ArrayList<UserResultObject> userResultList;
    UserInfo userDetail;


    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        arguments = getIntent().getExtras();
        userDetail = ApplicationEx.getAppInstance().getmUserInfo();
        userResultList = new ArrayList<>();

        if (arguments != null) {
            mUserRanking = (UserRanking) arguments.getSerializable(Constants.KEY_USERRANKING);
            userResultList = (ArrayList<UserResultObject>) arguments.getSerializable(Constants.KEY_USERRESULTLIST);
            setInfoToScreen();
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_edit:
                moveToEditProfileScreen();
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == Activity.RESULT_OK) {
                    userDetail = (UserInfo) data.getExtras().get(Constants.KEY_USERINFO);
                    setInfoToScreen();
                }
                break;
        }
    }

    @Click(R.id.ll_ranking_completelist)
    void onCompleteListClicked() {
        if (userResultList.size() > 0) {
            Intent completeListIntent = new Intent(MainProfileActivity.this, CompleteListActivity_.class);
            completeListIntent.putExtra(Constants.KEY_USERRESULTLIST, userResultList);
            startActivity(completeListIntent);
        } else {
            Snackbar.make(mRootLayout, "No Tests taken yet..", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.iv_profilepic)
    void onProfileImageClicked() {
        if (Tools.checkIfStringisValid(userDetail.getProfile_image())) {
            Intent imageViewIntent = new Intent(MainProfileActivity.this, ViewImageActivity_.class);
            imageViewIntent.putExtra(Constants.KEY_IMAGEURL, userDetail.getProfile_image());
            startActivity(imageViewIntent);
        }
    }

    @Click(R.id.ll_ranking_personalinfo)
    void onPersonalInfoClicked() {
        Intent personalInfoIntent = new Intent(MainProfileActivity.this, ProfileViewActivity_.class);
        startActivity(personalInfoIntent);
    }

    private void setInfoToScreen() {
        mTvNoOfQuiz.setText("" + mUserRanking.getTotal_no_quiz());
        mTvPersonalRating.setText("" + mUserRanking.getPersonal_rating());
        mTvWinPercentage.setText(mUserRanking.getWin_percentage() + " %");
        mTvNoofTimeWon.setText(mUserRanking.getNo_of_times_won() + "");
        if (userDetail != null) {
            mTvUserName.setText(userDetail.getFirst_name() + " " + userDetail.getLast_name());
        }

        if (Tools.checkIfStringisValid(userDetail.getProfile_image())) {
            Picasso.with(mContext).load(userDetail.getProfile_image()).placeholder(R.drawable.img_profile).into(mIvProfilePic);
        }

    }

    private void moveToEditProfileScreen() {
        Intent editProfileIntent = new Intent(MainProfileActivity.this, EditProfileActivity_.class);
        startActivityForResult(editProfileIntent, 0);
    }
}
