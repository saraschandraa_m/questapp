package com.smack.questapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by SaraschandraaM on 24/04/16.
 */
@EActivity(R.layout.activity_cropeventcover)
public class CropEventCoverActivity extends AppCompatActivity implements CropImageView.OnGetCroppedImageCompleteListener, CropImageView.OnSetImageUriCompleteListener {

    @ViewById(R.id.iv_crop_eventcover)
    CropImageView mIvEventCoverCrop;

    @ViewById(R.id.tl_eventcovercrop)
    Toolbar mToolbar;

    ArrayList<String> selectedPhotos;

    Bundle args;
    Context mContext;
    Bitmap croppedBitmap;

    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        mIvEventCoverCrop.setOnGetCroppedImageCompleteListener(this);
        mIvEventCoverCrop.setOnSetImageUriCompleteListener(this);
        args = getIntent().getExtras();
        selectedPhotos = new ArrayList<>();
        if (args != null) {
            selectedPhotos = args.getStringArrayList("selectedPhotos");
        }
        mIvEventCoverCrop.setImageBitmap(ApplicationEx.getAppInstance().decodeFile(selectedPhotos.get(0)));
    }

    private void onImageCropped() {
        Intent returnIntent = new Intent();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Log.i("SECOND CROPPING LENGTH", "" + byteArray.length);
        File croppedFile = savebitmap();
        String filepath = "" + croppedFile;
        returnIntent.putExtra("selectedPhoto", byteArray);
        returnIntent.putExtra("selectedPath", filepath);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crop, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_evcrop_crop:
                mIvEventCoverCrop.getCroppedImageAsync(mIvEventCoverCrop.getCropShape());
                break;

            case R.id.menu_evcrop_rotate:
                mIvEventCoverCrop.rotateImage(90);
                break;
        }

        return true;
    }

    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
            Bitmap sampleBitmap = bitmap;
            ByteArrayOutputStream sampleout = new ByteArrayOutputStream();
            sampleBitmap.compress(Bitmap.CompressFormat.JPEG, 80, sampleout);
            croppedBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(sampleout.toByteArray()));
            Log.i("Before Cropping LENGTH", "" + sampleout.toByteArray().length);
            Handler returnHandler = new Handler();
            returnHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onImageCropped();
                }
            }, 500);
        } else {
            Log.e("AIC", "Failed to crop image", error);
            Toast.makeText(mContext, "Image crop failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(mContext, "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("AIC", "Failed to load image by URI", error);
            Toast.makeText(mContext, "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private File savebitmap() {

        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/PhysicsSketchpad";
        File dir = new File(file_path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, "sketchpad" + ".png");
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            // make a new bitmap from your file
            Bitmap bitmap = croppedBitmap;

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("file", "" + file);
        return file;

    }
}
