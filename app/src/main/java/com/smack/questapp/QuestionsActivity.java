package com.smack.questapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import aynctask.WS_PostResult;
import model.Questions;
import model.Result;
import utils.Constants;
import utils.Urls;

/**
 * Created by SaraschandraaM on 28/06/16.
 */

@EActivity(R.layout.activity_questions)
public class QuestionsActivity extends AppCompatActivity {


    @ViewById(R.id.tv_timer)
    TextView mTvTimerText;

    @ViewById(R.id.tv_categoryname)
    TextView mTvCategoryName;

    @ViewById(R.id.tl_questions)
    Toolbar mToolbar;

    public static QuestionsActivity rootInstance;

    public static QuestionsActivity getRootInstance() {
        return rootInstance;
    }

    Context mContext;
    Bundle arguments;

    AlarmManager alarmManager;
    Intent refreshIntent;
    PendingIntent pendingRefreshIntent;
    FragmentTransaction mFragmentTransaction;

    QuestionsFragment questionsFragment;
    ResultFragment resultFragment;

    String sCategoryName, sUserID;
    public ArrayList<Questions> questionList;
    public int categoryID, noCorrectQuestions, duration;
    boolean isCancelTrue;
    CountDownTimer questionTimer;

    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        questionList = new ArrayList<>();
        arguments = getIntent().getExtras();
        questionList = (ArrayList<Questions>) arguments.getSerializable(Constants.KEY_QUESTION);
        duration = arguments.getInt(Constants.KEY_DURATION, 1);
        sCategoryName = arguments.getString(Constants.KEY_CATEGORY_NAME, "");
        categoryID = arguments.getInt(Constants.KEY_CATEGORY_ID, 0);
        sUserID = ApplicationEx.getAppInstance().getmUserInfo().getUser_id();

        mTvCategoryName.setText(sCategoryName);
        showTimer();
        changeQuestionPosition(0);
    }

    private void showTimer() {
        questionTimer = new CountDownTimer(1000 * 60 * duration, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                mTvTimerText.setText("" + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                mTvTimerText.setText("Time Up");
                changeToResult(true);
            }
        };

        questionTimer.start();
    }

    public void changeQuestionPosition(int position) {
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putInt(Constants.KEY_POSITION, position);
        questionsFragment = new QuestionsFragment_();
        try {
            questionsFragment.setArguments(args);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        mFragmentTransaction.replace(R.id.fl_questionholder, questionsFragment).commitAllowingStateLoss();
    }

    public void changeToResult(boolean isTimeUp) {
        questionTimer.cancel();
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        String timetaken = getTimeTaken(isTimeUp);
        int noWrongQuestion = questionList.size() - noCorrectQuestions;
        String params = Urls.POST_RESULT + sUserID + Urls.QUIZ_CATEGORY_ID + categoryID + Urls.NO_OF_QUESTIONS_ATTENDED + questionList.size()
                + Urls.NO_OF_CORRECT + noCorrectQuestions + Urls.NO_OF_WRONG + noWrongQuestion + Urls.MARKS + noCorrectQuestions + Urls.TIME_TAKEN + timetaken;


        new WS_PostResult(mContext).doSubmitResults(params, new WS_PostResult.PostResultListener() {
            @Override
            public void onResultsPosted(boolean isSuccess, Result result) {
                if (isSuccess) {
                    Bundle args = new Bundle();
                    args.putInt(Constants.KEY_NOQUESTION, questionList.size());
                    args.putInt(Constants.KEY_RESULT, noCorrectQuestions);
                    args.putString(Constants.KEY_CATEGORY_NAME, sCategoryName);
                    resultFragment = new ResultFragment_();
                    try {
                        resultFragment.setArguments(args);
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    mFragmentTransaction.replace(R.id.fl_questionholder, resultFragment).commitAllowingStateLoss();
                    mTvTimerText.setVisibility(View.GONE);
                    isCancelTrue = true;
                }
            }
        });
    }

    private String getTimeTaken(boolean isTimeUp) {
        String timetaken = "";
        if (!isTimeUp) {
            String text = mTvTimerText.getText().toString();
            String[] minsec = text.split(",");
            String[] min = minsec[0].split("min");
            String[] sec = minsec[1].split("sec");
            int minInt = Integer.parseInt(min[0].trim());
            int secInt = Integer.parseInt(sec[0].trim());
            int timeelpased = duration - minInt;
            int secondelapsed = 60 - secInt;
            Log.i("Time Elapsed " + timeelpased, "Second elapsed" + secondelapsed);
            String minText = String.format("%02d", timeelpased);
            timetaken = "00:" + minText + ":" + secondelapsed;
            Log.i("TimeTaken:", timetaken);
        } else {
            String minText = String.format("%02d", duration);
            timetaken = "00:" + minText + ":00";
        }

        return timetaken;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        rootInstance = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (questionTimer != null) {
            questionTimer.cancel();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (questionTimer != null) {
            questionTimer.cancel();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (isCancelTrue) {
            super.onBackPressed();
        } else {
            shouldCancelQuiz();
        }

    }

    public void shouldCancelQuiz() {
        AlertDialog.Builder cancelAlert = new AlertDialog.Builder(mContext);
        cancelAlert.setTitle("Alert");
        cancelAlert.setMessage("Are you sure you want to cancel the quiz?");
        cancelAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                isCancelTrue = true;
                dialogInterface.dismiss();
                onBackPressed();
            }
        });
        cancelAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                isCancelTrue = false;
                dialogInterface.dismiss();
            }
        });
        cancelAlert.show();
    }
}
