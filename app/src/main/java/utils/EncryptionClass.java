package utils;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

public class EncryptionClass {

	public String get_Convert_Base64(String data) {
		String base64String = new String(Base64.encode(data.getBytes()));
		System.out.println("Result of base64 **" + base64String);
		return base64String;
	}

	public String get_Convert_MD5key(String s) {
		String val = "";
		byte[] defaultBytes = s.getBytes();
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			val = hexString + "";
			System.out.println("pass " + val + "   md5 version is "
					+ hexString.toString());
		} catch (NoSuchAlgorithmException nsae) {
		}
		return val;
	}

	public ArrayList<String> orderAscDsc(ArrayList<String> staff_data, int i) {

		System.out.println("method reached ****" + staff_data.size());
		ArrayList<String> staff_Arrage = new ArrayList<String>();
		int kk = 0;
		try {
			for (int il = 0; il < staff_data.size(); il = il + 39) {
				String s = "";
				for (int l = 0; l < 39; l++) {

					s = s + staff_data.get(il + l) + "$";
				}
				staff_Arrage.add(s);
			}

			System.out.println("stage 1 ****");
			// Collections.sort(staff_Arrage, Collections.reverseOrder());
			Collections.sort(staff_Arrage);

			staff_data = new ArrayList<String>();
			System.out.println("compressed size" + staff_Arrage.size());

			kk = 0;
			for (int im = 0; im < staff_Arrage.size(); im++) {

				String val = staff_Arrage.get(im);

				String[] staffdata = val.split("$");

				for (String field : staffdata) {
					// System.out.println(field);
					staff_data.add(field);
					kk++;
				}

			}
			System.out.println("Total size*****" + staff_data.size());
			System.out.println("Ascending done*****");
		} catch (Exception e) {

		}
		return staff_data;

	}

}
