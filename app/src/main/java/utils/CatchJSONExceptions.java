package utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by SaraschandraaM on 25/05/16.
 */
public class CatchJSONExceptions implements Serializable {

    public String getStringFromJSON(JSONObject paramObject, String key) {
        String value = "";
        try {
            value = paramObject.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    public int getIntFromJSON(JSONObject paramObject, String key) {
        int value = 0;
        try {
            value = paramObject.getInt(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    public JSONArray getArrayFromJSON(JSONObject paramObject, String key) {
        JSONArray jsonArray = null;

        try {
            JSONObject json = paramObject.optJSONObject(key);
            if (json == null) {
                jsonArray = paramObject.getJSONArray(key);
            } else {
                jsonArray.put(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public JSONArray getArrayFromJSON_Nokey(JSONObject paramObject) {
        String response = paramObject.toString();
        String[] responsearray = response.split(",");
        Log.i("First Half", responsearray[0]);
        Log.i("Second Half", responsearray[1]);
        String result = responsearray[1];
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    public JSONObject getJSONObjectFromJSONArray(JSONArray jsonArray, int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonArray.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject getJSONFromString(String object) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject putValueToJSON(JSONObject jsonObject, String key, String value) {
        try {
            jsonObject.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
