package utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.smack.questapp.R;


/**
 * Created by SaraschandraaM on 22/08/15.
 */
public class WSLoadingDialog {

    Dialog wsLoadingDialog;

    public WSLoadingDialog(Context context) {
        wsLoadingDialog = new Dialog(context, R.style.AppTheme);
        View loadingView = LayoutInflater.from(context).inflate(R.layout.layout_loading_dialog, null);
        wsLoadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        wsLoadingDialog.getWindow().setBackgroundDrawableResource(R.color.color_alpha);
        wsLoadingDialog.setContentView(loadingView);
    }

    public void showWSLoadingDialog() {
        wsLoadingDialog.show();
    }

    public void hideWSLoadingDialog() {
        wsLoadingDialog.dismiss();
    }
}
