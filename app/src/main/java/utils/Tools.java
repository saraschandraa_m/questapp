package utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by SaraschandraaM on 03/09/15.
 */
public class Tools {


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isGPSAvailable(Context context) {
        boolean isGPS = false;
        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            isGPS = true;
        } else {
            isGPS = false;
        }
        return isGPS;
    }

    public static void expandViewVAnimator(View view, ValueAnimator mAnimator) {
        view.setVisibility(View.VISIBLE);
        mAnimator.start();
    }

    public static void collapseViewVAnimator(final View view) {
        int finalHeight = view.getHeight();
        ValueAnimator mAnimator = slideAnimator(finalHeight, 0, view);
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                view.setVisibility(View.GONE);                                                      // Height=0, but it set visibility to GONE
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }


    public static ValueAnimator slideAnimator(int start, int end, final View view) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = (Integer) valueAnimator.getAnimatedValue();                             // Update Height
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = value;
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    /**
     * Method to convert DP to PX
     *
     * @param mContext Context of the Activity/Fragment.
     * @param dp       Density Pixels of the respective View.
     * @return
     */
    public static int dpToPx(Context mContext, int dp) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String get12HourFormat(int hourOfDay, int minute) {
        String formattedTime = "", min;

        if (minute < 10) {
            min = "" + 0 + minute;
        } else {
            min = "" + minute;
        }

        int hour = hourOfDay % 12;
        if (hour == 0) {
            if (hourOfDay == 12) {
                formattedTime = "12" + ":" + min + "PM";
            } else if (hourOfDay == 24) {
                formattedTime = hour + ":" + min + "AM";
            }
        } else {
            formattedTime = hour + ":" + min;
            if (hourOfDay > 12) {
                formattedTime = formattedTime + "PM";
            } else {
                formattedTime = formattedTime + "AM";
            }
        }
        return formattedTime;
    }

    public static String getMonth(String Date) {
        String formattedMonth = "";
        java.util.Date eventDate = null;
        SimpleDateFormat dateMonthFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            eventDate = dateMonthFormat.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
        formattedMonth = monthFormat.format(eventDate);

        return formattedMonth;
    }

    public static String[] getFormattedDate(String Date) {
        String[] formattedDate = new String[3];
        java.util.Date eventDate = null;
        SimpleDateFormat dateMonthFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            eventDate = dateMonthFormat.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

        formattedDate[0] = dateFormat.format(eventDate);
        formattedDate[1] = monthFormat.format(eventDate);
        formattedDate[2] = yearFormat.format(eventDate);
        return formattedDate;
    }

    public static String[] getWinnerPeriod(String period) {
        String[] winPeriod = new String[2];
        String[] splitPeriod = period.split(" - ");
        String[] startPeriod = getFormattedDate(splitPeriod[0]);
        String[] endPeriod = getFormattedDate(splitPeriod[1]);

        if (startPeriod[2].equals(endPeriod[2])) {
            if (startPeriod[1].equals(endPeriod[1])) {
                winPeriod[0] = startPeriod[1] + " " + startPeriod[0] + " - " + endPeriod[0];
                winPeriod[1] = startPeriod[2];
            } else {
                winPeriod[0] = startPeriod[1] + " " + startPeriod[0] + " - " + endPeriod[1] + endPeriod[0];
                winPeriod[1] = startPeriod[2];
            }
        }

        return winPeriod;
    }

    public static String[] getFormattedProfileDate(String Date) {
        String[] formattedDate = new String[3];
        java.util.Date eventDate = null;
        SimpleDateFormat dateMonthFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            eventDate = dateMonthFormat.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

        formattedDate[0] = dateFormat.format(eventDate);
        formattedDate[1] = monthFormat.format(eventDate);
        formattedDate[2] = yearFormat.format(eventDate);
        return formattedDate;
    }

    public static boolean checkIfStringisValid(String stringToCheck) {
        boolean isStringValid = false;
        if (stringToCheck != null) {
            if (stringToCheck.equals("null") || stringToCheck.isEmpty()) {
                isStringValid = false;
            } else {
                isStringValid = true;
            }
        }
        return isStringValid;
    }

    public static boolean checkIfURLisValid(String stringToCheck) {
        boolean isStringValid = false;
        if (stringToCheck != null || stringToCheck.isEmpty()) {
            if (stringToCheck.equals("null") || stringToCheck.equals("Text") || stringToCheck.isEmpty()) {
                isStringValid = false;
            } else {
                isStringValid = true;
            }
        }
        return isStringValid;
    }
}
