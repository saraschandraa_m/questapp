package utils;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


public class Utils {


    public String getUdId(Context context) {
        // This will get the Real Device ID (IMEI NO)
        TelephonyManager TelephonyMgr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String strDeviceId = TelephonyMgr.getDeviceId(); // Requires

        return strDeviceId;

    }

    public boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            // boitealerte(this.getString(R.string.alert),
            // "getSystemService rend null");

        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {

                        return true;
                    }
                }
            }
        }
        return false;
    }

    // Validating the Email
    public boolean isValidEmail(String strEmail) {
        final Pattern EMAIL_ADDRESS_PATTERN = Pattern
                .compile("[a-zA-Z0-9+._%-+]{1,256}" + "@"
                        + "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" + "(" + "."
                        + "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" + ")+");

        if (EMAIL_ADDRESS_PATTERN.matcher(strEmail).matches()) {

            return true;
        } else {

            return false;
        }

    }


//    public String[] getLatLng(String strAddress, Context context) {
//
//        String address[] = new String[2];
//
//        if (Geocoder.isPresent()) {
//            try {
//                String location = strAddress;
//                Geocoder gc = new Geocoder(context);
//                List<Address> addresses = gc.getFromLocationName(location, 5); // get the found Address Objects
//
//                List<LatLng> ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
//                for (Address a : addresses) {
//                    if (a.hasLatitude() && a.hasLongitude()) {
//                        ll.add(new LatLng(a.getLatitude(), a.getLongitude()));
//                        address[0] = a.getLatitude() + "";
//                        address[1] = a.getLongitude() + "";
//                        Log.d("", "lat lng  ---->" + ll);
//
//                        if (!ll.isEmpty()) {
//
//                            Log.d("", "Lat---------->" + a.getLatitude());
//                            Log.d("", "Lng---------->" + a.getLongitude());
//
//                        }
//
//                    }
//                }
//            } catch (IOException e) {
//                // handle the exception
//            }
//        }
//        return address;
//    }


    public boolean isDateAfterDay(String startDate, String endDate) {
        try {

            String myFormatString = "yyyy-MM-dd"; // for example

            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

//            if (date1.compareTo(startingDate) == 0)    // equal
//                return true;
//            else

            if (date1.compareTo(startingDate) > 0)    //greater
                return true;
            else                                        //lesser
                return false;

        } catch (Exception e) {
            System.out.println("ERROR  " + e.getMessage());
            return false;
        }
    }

    public boolean isDateAfter(String startDate, String endDate) {
        try {

            String myFormatString = "yyyy-MM-dd HH:mm:ss"; // for example

            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

//            if (date1.compareTo(startingDate) == 0)    // equal
//                return true;
//            else

            if (date1.compareTo(startingDate) > 0)    //greater
                return true;
            else                                        //lesser
                return false;

        } catch (Exception e) {
            System.out.println("ERROR  " + e.getMessage());
            return false;
        }
    }


    public boolean isDateEqualOrGreaterUpdate(String startDate, String endDate) {//arun


        Log.d("Utils", "Arun Start Date " + startDate + "End Date ---->" + endDate);

        try {

            String myFormatString = "yyyy-MM-dd HH:mm:ss";

            SimpleDateFormat df = new SimpleDateFormat(myFormatString);

            Date startingDate = df.parse(startDate);
            Date date1 = df.parse(endDate);

            if (date1.compareTo(startingDate) == 0)    // equal
                return true;
            else if (date1.compareTo(startingDate) > 0)    //greater
                return true;
            else                                        //lesser
                return false;

        } catch (Exception e) {
            System.out.println("ERROR  " + e.getMessage());
            return false;
        }
    }


    public boolean isDateEqualOrGreater(String startDate, String endDate) {
        try {

            String myFormatString = "yyyy-MM-dd HH:mm:ss"; // for example

            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            if (date1.compareTo(startingDate) == 0)    // equal
                return true;
            else if (date1.compareTo(startingDate) > 0)    //greater
                return true;
            else                                        //lesser
                return false;

        } catch (Exception e) {
            System.out.println("ERROR  " + e.getMessage());
            return false;
        }
    }


    public String convertToMin(String stHours) {

        int intMin = 0;
        String strHours = "";

        Calendar cal = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        try {

            Date date = sdf.parse(stHours);

            cal.setTime(date);

            intMin = cal.get(Calendar.HOUR) * 60 + cal.get(Calendar.MINUTE);


        } catch (Exception e) {

            e.printStackTrace();
        }

        Log.d("", "Arun--------Minutes" + intMin);

        if (intMin == 1) {

            strHours = "minutes";

        }
        if (intMin < 60 && intMin > 1) {

            strHours = "mins";

        }
        if (intMin == 60) {

            strHours = "hr";

        }
        if (intMin > 60) {

            strHours = "hrs";

        }


        return strHours;
    }


    public int getQuestionTotal(ArrayList<Integer> arrChapterCount) {
        int sum = 0;
        for (int i = 0; i < arrChapterCount.size(); i++) {
            sum += arrChapterCount.get(i);
        }
        return sum;
    }


    public String getDateFormate(String date) {

        String str = null;

        Date dateSpecified = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);//
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            dateSpecified = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        str = dateFormatter.format(dateSpecified);

        return str;
    }


    public String getDateFmString(String date) {

        String str = null;
        Date dateSpecified = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM dd, yyyy", Locale.US);//MMM dd, yyyy
        try {
            dateSpecified = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        str = dateFormatter.format(dateSpecified);
        return str;
    }


    public String getTimeFmString(String date) {

        String str = null;
        Date dateSpecified = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm a", Locale.US);//MMM dd, yyyy
        try {
            dateSpecified = dateFormat.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        str = dateFormatter.format(dateSpecified);

        String formatDate1 = str.replace("AM", "am").replace("PM", "pm");

        return formatDate1;
    }


    public String getDateFromString(String date) {

        String str = null;

        Date dateSpecified = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);//yyyy-MM-dd
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try {
            dateSpecified = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Log.d("","Arun--------dateSpecified"+dateSpecified);

        //System.out.println(dateSpecified);

        str = dateFormatter.format(dateSpecified);

        return str;
    }


    public String getDateForSearch(String date) {

        String str = null;

        Date dateSpecified = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);//yyyy-MM-dd
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            dateSpecified = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Log.d("","Arun--------dateSpecified"+dateSpecified);

        //System.out.println(dateSpecified);

        str = dateFormatter.format(dateSpecified);

        return str;
    }


    public void showToast(Context context, String msg) {

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    public boolean isSimAvailable(Context contxt) {
        TelephonyManager tm = (TelephonyManager) contxt
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            return true;
        } else {
            return false;
        }
    }


//    public boolean isValidPhoneNumber(String number, String countryCode) {
//        boolean isVallid = false;
//
//        try {
//
//            PhoneNumberUtil phoneUtil = PhoneNumberUtil
//                    .getInstance();
//            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(number, countryCode);
//
//            isVallid = phoneUtil.isValidNumber(numberProto);
//
//            Log.d("NumberValid", String.valueOf(isVallid));
//
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//        return isVallid;
//
//    }


    //***********File upload

//    public String uploadFile(final String strUrl, String sourceFileUri, String strUserId, String strImageType) {
//
//        String fileName = sourceFileUri;
//
//        HttpURLConnection conn = null;
//        DataOutputStream dos = null;
//        String lineEnd = "\r\n";
//        String twoHyphens = "--";
//        String boundary = "*****";
//        int bytesRead, bytesAvailable, bufferSize;
//        byte[] buffer;
//        int maxBufferSize = 1 * 1024 * 1024;
//        File sourceFile = new File(sourceFileUri);
//
//        String mStrUrl = "strarun";
//        String mStrReturn = "";
//
//        try {
//
//            // open a URL connection to the Servlet
//            FileInputStream fileInputStream = new FileInputStream(sourceFile);
//
//            String loadurl = strUrl + strUserId + Urls.PRACT_IMAGE_IMAGE_TYPE + strImageType;// url
//
//            Log.e("", "Center loadurl " + loadurl);
//
//            URL url = new URL(loadurl);
//
//            Log.d("<<<<<<Profile>>>>>>>>", "id with url in async--------->"
//                    + loadurl);
//
//            // Open a HTTP connection to the URL
//            conn = (HttpURLConnection) url.openConnection();
//
//            Log.d("<<<Profile>>>", "successfully crossed2");
//
//            conn.setDoInput(true); // Allow Inputs
//            conn.setDoOutput(true); // Allow Outputs
//            conn.setUseCaches(false); // Don't use a Cached Copy
//            Log.d("<<<Profile>>>", " successfullcrossed3");
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Connection", "Keep-Alive");
//            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
//
//            conn.setRequestProperty("Content-Type",
//                    "multipart/form-data;boundary=" + boundary);
//
//            conn.setRequestProperty("student_profile", fileName);
//
//            dos = new DataOutputStream(conn.getOutputStream());
//            Log.d("<<<Profile>>>", " successfully crossed4");
//
//            dos.writeBytes(twoHyphens + boundary + lineEnd);
//
//            dos.writeBytes("Content-Disposition:form-data; name=\"file\"; filename=\"image.jpg\""
//                    + lineEnd);
//
//            dos.writeBytes(lineEnd);
//
//            // create a buffer of maximum size
//            bytesAvailable = fileInputStream.available();
//
//            bufferSize = Math.min(bytesAvailable, maxBufferSize);
//            buffer = new byte[bufferSize];
//
//            // read file and write it into form...
//            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//
//            while (bytesRead > 0) {
//
//                dos.write(buffer, 0, bufferSize);
//                bytesAvailable = fileInputStream.available();
//                bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//
//            }
//            Log.d("<<<Profile>>>", " successfully crossed6");
//            // send multipart form data necesssary after file data...
//            dos.writeBytes(lineEnd);
//            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
//            Log.d("<<<Profile>>", " successfully crossed7");
//            // Responses from the server (code and message)
//            // serverResponseCode = conn.getResponseCode();
//
//            // String json =dos.
//            // Log.d("", "OWC Response---------->" + json);
//            String serverResponseMessage = conn.getResponseMessage();
//            Log.d("<<<Profile>>>", " successfully crossed8");
//
//            Log.i("uploadFile", "HTTP Response is : "
//                    + serverResponseMessage + ": " + serverResponseMessage);
//
//            DataInputStream inStream = new DataInputStream(
//                    conn.getInputStream());
//
//            Log.d("<<Profile>>>", " successfully crossed ");
//            // String str;
//
//            while ((mStrUrl = inStream.readLine()) != null) {
//                Log.e("Debug", "Server Response for OWC-------> " + mStrUrl);
//                if (!mStrUrl.equalsIgnoreCase("owc029")) {
//
////                    prefLoginResponse = ProfileActivity.this
////                            .getSharedPreferences("prefs_login_response",
////                                    MODE_WORLD_READABLE);
////
////                    prefLoginResponseEditor = prefLoginResponse.edit();
////                    prefLoginResponseEditor.putString("image", mStrUrl);
////                    prefLoginResponseEditor.commit();
//
//                    Log.d("Debug",
//                            "uploadFile Server Respone for OWC After pref-------> "
//                                    + mStrUrl);
//                    Log.e("", "Center Respone " + mStrUrl);
//                    mStrReturn = mStrUrl;
//
//                }
//
//            }
//
//            inStream.close();
//
//            // close the streams //
//            fileInputStream.close();
//            dos.flush();
//            dos.close();
//
//        } catch (MalformedURLException ex) {
//
//            ex.printStackTrace();
//
//            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//            // Log.e("Upload file to server Exception","Exception : " + e.getMessage(), e.printStackTrace());
//
//        }
//
//        return mStrReturn;
//
//    }

    public static void downloadImageFromUrl(Context context, String mstrUploadReturn) {


        try {
            // set the download URL, a url that points to a file on the
            // internet
            // this is the file to be downloaded
            Log.d("Utils", "File url1------------>" + mstrUploadReturn);

            URL uploadReturnUrl = new URL(mstrUploadReturn);

            Log.d("Utils", "File url------------>" + mstrUploadReturn);

            // create the new connection
            HttpURLConnection urlConnection = (HttpURLConnection) uploadReturnUrl
                    .openConnection();

            // set up some things on the connection
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            // and connect!
            urlConnection.connect();

            // set the path where we want to save the file
            // in this case, going to save it on the root directory of the

            File mFolder = new File(context.getFilesDir() + "/mylocum");// ex

            // create a new file, specifying the path, and the filename
            // which we want to save the file as.

            File file = new File(mFolder.getAbsoluteFile(), "mylocumprofile.png");// ex

            Log.d("Utils", "File SdcardRootarun-- file--------->" + file);

            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            if (!file.exists()) {
                file.createNewFile();
            }

            // this will be used to write the downloaded data into the file
            // we created
            FileOutputStream fileOutput = new FileOutputStream(file);

            // this will be used in reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            // this is the total size of the file
            int totalSize = urlConnection.getContentLength();
            // variable to store total downloaded bytes
            int downloadedSize = 0;

            // create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0; // used to store a temporary size of the
            // buffer

            // now, read through the input buffer and write the contents to
            // the file
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                // add the data in the buffer to the file in the file output
                // stream (the file on the sd card
                fileOutput.write(buffer, 0, bufferLength);
                // add up the size so we know how much is downloaded
                downloadedSize += bufferLength;
                // this is where you would do something to report the
                // prgress, like this maybe
                // updateProgress(downloadedSize, totalSize);

            }
            // close the output stream when done
            fileOutput.close();

            // catch some possible errors...
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static void ShowPicture(Context context, String fileName, ImageView pic) {

        File mFolder = new File(context.getFilesDir() + "/mylocum");

        File f = new File(mFolder.getAbsoluteFile(), "mylocumprofile.png");

        FileInputStream is = null;
        try {

            is = new FileInputStream(f);

        } catch (FileNotFoundException e) {

            Log.d("error: ", String.format(
                    "ShowPicture.java file[%s]Not Found", fileName));

            return;
        }

        Bitmap bm = BitmapFactory.decodeStream(is, null, null);

        if (bm != null) {

            pic.setImageBitmap(bm);

        }

        Log.d("ProfileActivity", "file bitmap---->" + bm);

    }

    public static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    public String getFormatDate(String strDate) {
        // 2015-11-25 12:00:00
// 2016-01-22 5:05 AM
//        SimpleDateFormat sourceFormat = new SimpleDateFormat(
//                "yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat sourceFormat = new SimpleDateFormat(
                "yyyy-MM-dd hh:mm a");

        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "MMM dd, yyyy hh:mm a");
//        desiredFormat.setTimeZone(TimeZone.getDefault());
//        // 'a' for AM/PM
//        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = sourceFormat.parse(strDate);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        String formattedDate1 = desiredFormat.format(date.getTime()).replace("AM", "am").replace("PM", "pm");

//        Log.e("getFormattedDate","formattedDate1 "+formattedDate1);

        return formattedDate1.toString();
    }


    public String get12HoursFormat(String _24HourTime) {
        String formatDate = "";
        Log.e("Utils ", "_24HourTime " + _24HourTime);
        try {

            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            formatDate = _12HourSDF.format(_24HourDt);
            System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String formatDate1 = formatDate.replace("AM", "am").replace("PM","pm");
        Log.e("Utils ", "get12HoursFormat " + formatDate);
        return formatDate;
    }

    public String get24HoursFormat(String _24HourTime) {
        String formatDate = "";
        Log.e("Utils ", "_24HourTime " + _24HourTime);
        try {

            SimpleDateFormat _twevelveHourSDF = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");

            Date _12HourDt = _twevelveHourSDF.parse(_24HourTime);

            formatDate = _24HourSDF.format(_12HourDt);

            System.out.println(_twevelveHourSDF);
            System.out.println(_24HourSDF.format(_twevelveHourSDF));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String formatDate1 = formatDate.replace("AM", "am").replace("PM","pm");
        Log.e("Utils ", "get24HoursFormat " + formatDate);
        return formatDate;
    }

    public String getDateFormat(String monthFormat) {
        String formatDate = "";


        Log.e("Utils ", "_24HourTime " + monthFormat);
        try {

            SimpleDateFormat sourceFormat = new SimpleDateFormat(
                    "yyyy-MM-dd");


            SimpleDateFormat desiredFormat = new SimpleDateFormat(
                    "MMM dd, yyyy");
            Date _24HourDt = sourceFormat.parse(monthFormat);
            formatDate = desiredFormat.format(_24HourDt);

        } catch (Exception e) {
            e.printStackTrace();
        }
//        String formatDate1 = formatDate.replace("AM", "am").replace("PM","pm");

        Log.e("Utils ", "getDateFormat " + formatDate);
        return formatDate;
    }

    public String getNormalDateFormat(String monthFormat) {
        String formatDate = "";

        try {

            SimpleDateFormat sourceFormat = new SimpleDateFormat(
                    "MMM dd, yyyy");


            SimpleDateFormat desiredFormat = new SimpleDateFormat(
                    "yyyy-MM-dd");
            Date _24HourDt = sourceFormat.parse(monthFormat);
            formatDate = desiredFormat.format(_24HourDt);

        } catch (Exception e) {
            e.printStackTrace();
        }
//        String formatDate1 = formatDate.replace("AM", "am").replace("PM","pm");

        Log.e("Utils ", "getDateFormat " + formatDate);
        return formatDate;
    }


    public int ListSelectedCalendars(String eventtitle, Context context) {


        Uri eventUri;
        if (android.os.Build.VERSION.SDK_INT <= 7) {
            // the old way

            eventUri = Uri.parse("content://calendar/events");
        } else {
            // the new way

            eventUri = Uri.parse("content://com.android.calendar/events");
        }

        int result = 0;
        String projection[] = {"_id", "title"};
        Cursor cursor = context.getContentResolver().query(eventUri, null, null, null,
                null);

        if (cursor.moveToFirst()) {

            String calName;
            String calID;

            int nameCol = cursor.getColumnIndex(projection[1]);
            int idCol = cursor.getColumnIndex(projection[0]);
            do {
                calName = cursor.getString(nameCol);
                calID = cursor.getString(idCol);

                if (calName != null && calName.contains(eventtitle)) {
                    result = Integer.parseInt(calID);
                }

            } while (cursor.moveToNext());
            cursor.close();
        }

        return result;

    }


    public int DeleteCalendarEntry(int entryID, Context context) {
        int iNumRowsDeleted = 0;

        Uri eventUri = ContentUris
                .withAppendedId(getCalendarUriBase(), entryID);
        iNumRowsDeleted = context.getContentResolver().delete(eventUri, null, null);

        return iNumRowsDeleted;
    }

    private Uri getCalendarUriBase() {
        Uri eventUri;
        if (android.os.Build.VERSION.SDK_INT <= 7) {
            // the old way

            eventUri = Uri.parse("content://calendar/events");
        } else {
            // the new way

            eventUri = Uri.parse("content://com.android.calendar/events");
        }

        return eventUri;
    }

    public long changeToMilliSecond(String strDate) {
        String string_date = strDate;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        Date d = null;
        try {
            d = f.parse(string_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long milliseconds = 0;
        if (d != null) {
            milliseconds = d.getTime();
        }

        return milliseconds;
    }

    public long pushAppointmentsToCalender(Activity curActivity, String title, String addInfo, String place, int status, long startDate, long lgEndDate, boolean needReminder, boolean needMailService) {
        /***************** Event: note(without alert) *******************/
        String eventUriString = "content://com.android.calendar/events";
        ContentValues eventValues = new ContentValues();

        eventValues.put("calendar_id", 1); // id, We need to choose from our mobile for primary its 1
        eventValues.put("title", title);
        eventValues.put("description", addInfo);
        eventValues.put("eventLocation", place);
        long endDate = startDate + 1000 * 60 * 60; // For next 1hr
        //  long endDate = lgEndDate; // For next 1hr
        eventValues.put("dtstart", startDate);
        eventValues.put("dtend", endDate);
        eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, "Indian/Christmas");  // Here choose your location time zone
        // values.put("allDay", 1); //If it is bithday alarm or such
        // kind (which should remind me for whole day) 0 for false, 1
        // for true
        eventValues.put("eventStatus", status); // This information is
        // sufficient for most entries tentative(0),confirmed(1) or canceled(2):

        /*Comment below visibility and transparency  column to avoid java.lang.IllegalArgumentException column visibility is invalid error
        eventValues.put("visibility", 3); // visibility to default (0),
        // confidential (1), private
        // (2), or public (3):
        eventValues.put("transparency", 0); // You can control whether
        /*
        an event consumes time
        opaque (0) or transparent
        (1).
        */

        eventValues.put("hasAlarm", 1); // 0 for false, 1 for true
        if (ActivityCompat.checkSelfPermission(curActivity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return startDate;
        }
        Uri eventUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(eventUriString), eventValues);

//        assert eventUri != null;
        long eventID = Long.parseLong(eventUri.getLastPathSegment());
        Log.e("", "eventUri " + eventUri.getLastPathSegment());

        if (needReminder) {
            /***************** Event: Reminder(with alert) Adding reminder to event *******************/

            String reminderUriString = "content://com.android.calendar/reminders";

            ContentValues reminderValues = new ContentValues();

            reminderValues.put("event_id", eventID);

            //2 hours before alarm
            reminderValues.put("minutes", 120); // Default value of the
            // system. Minutes is a
            // integer
            reminderValues.put("method", 1); // Alert Methods: Default(0),
            // Alert(1), Email(2),
            // SMS(3)

            Uri reminderUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(reminderUriString), reminderValues);
        }

        /***************** Event: Meeting(without alert) Adding Attendies to the meeting *******************/

        if (needMailService) {
            String attendeuesesUriString = "content://com.android.calendar/attendees";

            /********
             * To add multiple attendees need to insert ContentValues multiple
             * times
             ***********/
            ContentValues attendeesValues = new ContentValues();

            attendeesValues.put("event_id", eventID);
            attendeesValues.put("attendeeName", "xxxxx"); // Attendees name
            attendeesValues.put("attendeeEmail", "yyyy@gmail.com");// Attendee
            // E
            // mail
            // id
            attendeesValues.put("attendeeRelationship", 0); // Relationship_Attendee(1),
            // Relationship_None(0),
            // Organizer(2),
            // Performer(3),
            // Speaker(4)
            attendeesValues.put("attendeeType", 0); // None(0), Optional(1),
            // Required(2), Resource(3)
            attendeesValues.put("attendeeStatus", 0); // NOne(0), Accepted(1),
            // Decline(2),
            // Invited(3),
            // Tentative(4)

            Uri attendeuesesUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(attendeuesesUriString), attendeesValues);
        }

        return eventID;
    }

}
