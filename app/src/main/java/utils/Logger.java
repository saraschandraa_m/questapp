package utils;

import android.util.Log;

/**
 * Created by SaraschandraaM on 23/10/16.
 */

public class Logger {

    public static void showInfo(String TAG, String result) {
        largeLog(TAG, result);
    }


    public static void largeLog(String tag, String content) {
        if (content.length() > 4000) {
            printInfo(tag, content.substring(0, 4000));
            largeLog(tag, content.substring(4000));
        } else {
            printInfo(tag, content);
        }
    }


    private static void printInfo(String TAG, String result) {
        Log.i(TAG, result);
    }
}
