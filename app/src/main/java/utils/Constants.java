package utils;

/**
 * Created by SaraschandraaM on 28/06/16.
 */
public interface Constants {

    public String PREF_USERNAME = "username";
    public String PREF_PASSWORD = "password";
    public String PREF_ISLOGGED = "isloggedin";
    public String PREF_USERDETAILS = "userdetails";
    public String PREG_GCMREGID = "gcm_key";

    public String KEY_CATEGORY = "category";
    public String KEY_EMAIL = "email";
    public String KEY_FROMFB = "fromFB";
    public String KEY_FBID = "facebookid";
    public String KEY_DURATION = "duration";
    public String KEY_POSITION = "position";
    public String KEY_QUESTION = "question";
    public String KEY_CATEGORY_NAME = "categoryname";
    public String KEY_CATEGORY_ID = "categoryid";
    public String KEY_NOQUESTION = "noofquest";
    public String KEY_RESULT = "result";
    public String DATE_DISPLAY = "date_display";
    public String DATE_PARAM = "date_param";
    public String KEY_WINNERLIST = "winner_list";
    public String KEY_USERRANKING = "user_ranking";
    public String KEY_USERRESULTLIST = "user_result_list";
    public String KEY_USERINFO = "user_detail";
    public String KEY_IMAGEURL = "image_url";
    public String KEY_USERRESULTOBJ = "user_result_object";
    public String KEY_USERID = "user_id";


    public static final String GOOGLE_PROJ_ID = "56556221789";

    public String TWITTER_KEY = "bo6Cv19kemImZ7WMWnXzRElOg";
    public String TWITTER_SECRET = "sfDUIvTNJA3bBDRrM0JlVY6Y5Xi3aMYibkNgyPu8ztdqBNS6cF";
}
