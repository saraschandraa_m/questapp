package utils;

public class Urls {


    //PASSCODE
    public static final String PASSCODE = "&passcode=3BE45B51A8FE67A7778D245661131";
    public static final String BASE_URL = "http://quiz.thedevelopersolutions.com/api1/?";

    public static final String REGISTER_1_GCM_ID = "action=registration&gcmregid=";
    public static final String REGISTER_2_FIRST_NAME = "&first_name=";
    public static final String REGISTER_3_LAST_NAME = "&last_name=";
    public static final String REGISTER_4_PASSWORD = "&password=";
    public static final String REGISTER_5_EMAIL = "&email_id=";
    public static final String REGISTER_6_LOCATION = "&location=";
    public static final String REGISTER_7_COLLEGE_NAME = "&college_name=";
    public static final String REGISTER_8_PHONE_NUMBER = "&phone=";
    public static final String REGISTER_9_DATE_OF_BIRTH = "&date_of_birth=";
    public static final String REGISTER_10_GENDER = "&gender=";

    public static final String LOGIN_1_EMAIL_ID = "action=login&email_id=";
    public static final String LOGIN_2_PASSWORD = "&password=";
    public static final String LOGIN_3_GCM_ID = "&gcmregid=";

    public static final String CATEGORY = "action=get_categories&user_id=";

    public static final String QUESTIONS = "action=get_questions&user_id=";
    public static final String QUIZ_CATEGORY_ID = "&quiz_category_id=";

    public static final String FACEBOOK_LOGIN = "action=facebook_login&email_id=";
    public static final String FACEBOOK_LOGIN_1 = "&gcmregid=";

    public static final String FB_REGISTRATION = "action=facebook_registration&gcmregid=";
    public static final String FB_FBID = "&facebook_id=";

    public static final String POST_RESULT = "action=update_quiz_result&user_id=";
    public static final String NO_OF_QUESTIONS_ATTENDED = "&no_of_question_attended=";
    public static final String NO_OF_CORRECT = "&no_of_correct=";
    public static final String NO_OF_WRONG = "&no_of_wrong=";
    public static final String MARKS = "&marks=";
    public static final String TIME_TAKEN = "&time_taken=";

    public static final String GET_WINNER_LIST = "action=winner_list&user_id=";

    public static final String GET_USER_RANKING = "action=get_user_ranking&user_id=";

    public static final String GET_USER_RESULTS = "action=user_results&user_id=";

    public static final String UPDATE_PROFILE = "action=update_profile&user_id=";
    public static final String UPDATE_PROFILE_FIRSTNAME = "&first_name=";
    public static final String UPDATE_PROFILE_LASTNAME = "&last_name=";
    public static final String UPDATE_PROFILE_CONTACTNO = "&contact_no=";
    public static final String UPDATE_PROFILE_GENDER = "&gender=";
    public static final String UPDATE_PROFILE_COLLEGENAME = "&college_name=";
    public static final String UPDATE_PROFILE_DOB = "&dob=";
    public static final String UPDATE_PROFILE_LOCATION = "&location=";

}
