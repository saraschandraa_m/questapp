package utils;



import webservices.ServiceResponse;

/**
 * Created by arun.r on 12/5/15.
 */
public class Singleton{

    private static final EncryptionClass ENCRYPTION_CLASS = new EncryptionClass();

    private static final Utils UTILS = new Utils();

    private static final ServiceResponse SERVICE_RESPONSE = new ServiceResponse();

    public Singleton(){

    }

    public static EncryptionClass getEncryption() {

        return ENCRYPTION_CLASS;
    }

    public static Utils getUtils(){

        return UTILS;
    }

    public static ServiceResponse getWebservices(){

        return  SERVICE_RESPONSE;
    }





}
