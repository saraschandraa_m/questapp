package utils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.smack.questapp.R;

import listeners.ShareClickListener;

/**
 * Created by SaraschandraaM on 22/12/16.
 */

public class SocialMediaShareDialog {

    Dialog shareDialog;
    LinearLayout mLlShareFB, mLlShareTwitter;

    public SocialMediaShareDialog(Context context) {
        shareDialog = new Dialog(context, R.style.AppTheme);
        View loadingView = LayoutInflater.from(context).inflate(R.layout.dialog_share, null);
        mLlShareFB = (LinearLayout) loadingView.findViewById(R.id.ll_share_fb);
        mLlShareTwitter = (LinearLayout) loadingView.findViewById(R.id.ll_share_twitter);
        shareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shareDialog.getWindow().setBackgroundDrawableResource(R.color.color_alpha);
        shareDialog.setContentView(loadingView);

    }

    public void showDialog(final ShareClickListener shareClickListener) {
        shareDialog.show();
        mLlShareFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareClickListener.onShareClicked(0);
                shareDialog.dismiss();
            }
        });

        mLlShareTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareClickListener.onShareClicked(1);
                shareDialog.dismiss();
            }
        });
    }
}
