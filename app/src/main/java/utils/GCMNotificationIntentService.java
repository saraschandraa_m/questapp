package utils;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

//import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.smack.questapp.R;
import com.smack.questapp.SplashActivity;
import com.smack.questapp.SplashActivity_;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by SaraschandraaM on 14/04/16.
 */
public class GCMNotificationIntentService extends IntentService {

    public static final int notifyID = 9001;
    NotificationCompat.Builder builder;
    CatchJSONExceptions catchJSONExceptions;
    int count = 0;
    static String GCMNotif = "";

    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
//        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
//
//        String messageType = gcm.getMessageType(intent);
//
//        if (!extras.isEmpty()) {
//            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//                sendNotification("Send error: " + extras.toString());
//
//            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
//                sendNotification("Deleted messages on server: "
//                        + extras.toString());
//            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//                Log.i("Complete Response", extras.toString());
//                sendNotification(extras.getString("message"));
//            }
//        }
        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String msg) {
        if (GCMNotif.isEmpty() && !GCMNotif.equals(msg)) {
//            if (!ApplicationEx.getAppInstance().isGCMCreator) {
            GCMNotif = msg;
            catchJSONExceptions = new CatchJSONExceptions();
            JSONObject gcmJSON = catchJSONExceptions.getJSONFromString(msg);
//            GCMObject gcmObject = new GCMObject(gcmJSON);

//                if (!gcmObject.getEimage().isEmpty() || !gcmObject.getEimage().equals("null")) {
//                    new sendNotificationImage(this, gcmObject).execute();
//                } else {
//                    showNotification(gcmObject, null);
//                }
//            } else {
//                ApplicationEx.getAppInstance().isGCMCreator = false;
//            }
            showNotification();
            Log.i("GCM_RESPONSE", msg);
        }


    }

    private static int getNotificationIcon() {
//        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.mipmap.ic_notification : R.mipmap.ic_launcher;//ic_launcher_not_set_due_to_6.0
        return R.mipmap.ic_launcher;
    }

//    private String getNotificationMessage(GCMObject gcmObject) {
//        String message = "";
//
//        switch (gcmObject.getType()) {
//            case "Event":
//                message = "Event " + gcmObject.getTitle() + " has been Created.";
//                break;
//
//            case "Joined":
//                message = gcmObject.getName() + " has joined " + gcmObject.getEName();
//                break;
//
//            case "Follow":
//                message = gcmObject.getName() + "has followed " + gcmObject.getEName();
//                break;
//        }
//
//        return message;
//    }

//    private class sendNotificationImage extends AsyncTask<String, Void, Bitmap> {
//
//        Context ctx;
//        String message;
//        GCMObject gcmObject;
//
//        public sendNotificationImage(Context context, GCMObject gcmObject) {
//            super();
//            this.ctx = context;
//            this.gcmObject = gcmObject;
//        }
//
//        @Override
//        protected Bitmap doInBackground(String... params) {
//
//            InputStream in;
////            message = params[0] + params[1];
//            try {
//                URL url = new URL(gcmObject.getEimage());
//                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                in = connection.getInputStream();
//                Bitmap myBitmap = BitmapFactory.decodeStream(in);
//                return myBitmap;
//
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap result) {
//
//            super.onPostExecute(result);
//            try {
////                NotificationManager notificationManager = (NotificationManager) ctx
////                        .getSystemService(Context.NOTIFICATION_SERVICE);
////
////                Intent intent = new Intent(ctx, NotificationsActivity.class);
////                intent.putExtra("isFromBadge", false);
////
////
////                Notification notification = new Notification.Builder(ctx)
////                        .setContentTitle(
////                                ctx.getResources().getString(R.string.app_name))
////                        .setContentText(message)
////                        .setSmallIcon(R.drawable.ic_launcher)
////                        .setLargeIcon(result).build();
////
////                // hide the notification after its selected
////                notification.flags |= Notification.FLAG_AUTO_CANCEL;
////
////                notificationManager.notify(1, notification);
//                showNotification(gcmObject, result);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

//    private void showNotification(GCMObject gcmObject, Bitmap bitmap) {
//        Intent resultIntent = new Intent(this, EventsHomeActivity_.class);
//        resultIntent.putExtra("isFromGCM", true);
//        resultIntent.putExtra("gcmname", gcmObject.getName());
//        resultIntent.putExtra("gcmtype", gcmObject.getType());
//        resultIntent.putExtra("gcmEimage", gcmObject.getEimage());
//        resultIntent.putExtra("gcmImage", gcmObject.getImage());
//        resultIntent.putExtra("gcmEventId", gcmObject.getEventId());
////        resultIntent.putExtra("gcmEventTitle", );
//        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);
//
//
//        NotificationCompat.Builder mNotifyBuilder;
//        NotificationManager mNotificationManager;
//
//        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        mNotifyBuilder = new NotificationCompat.Builder(this)
//                .setContentTitle("TeensMAD")
//                .setContentText(getNotificationMessage(gcmObject))
//                .setSmallIcon(getNotificationIcon());
//        // Set pending intent
//        mNotifyBuilder.setContentIntent(resultPendingIntent);
//        if (bitmap != null) {
//            mNotifyBuilder.setLargeIcon(bitmap);
//        }
//        // Set Vibrate, Sound and Light
//        int defaults = 0;
//        defaults = defaults | Notification.DEFAULT_LIGHTS;
//        defaults = defaults | Notification.DEFAULT_VIBRATE;
//        defaults = defaults | Notification.DEFAULT_SOUND;
//
//        mNotifyBuilder.setDefaults(defaults);
//        // Set the content for Notification
//        // Set autocancel
//        mNotifyBuilder.setAutoCancel(true);
//        // Post a notification
//        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
//    }

    private void showNotification() {
        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;

        Intent resultIntent = new Intent(this, SplashActivity_.class);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("QuestApp")
                .setContentText("You have recieved a new notification")
                .setSmallIcon(getNotificationIcon());
        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);
//        if (bitmap != null) {
//            mNotifyBuilder.setLargeIcon(bitmap);
//        }
        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        // Set the content for Notification
        // Set autocancel
        mNotifyBuilder.setAutoCancel(true);
        // Post a notification
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }
}
