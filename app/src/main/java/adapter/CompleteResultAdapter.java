package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smack.questapp.R;

import java.util.ArrayList;

import model.UserResultObject;
import utils.Tools;

/**
 * Created by SaraschandraaM on 09/11/16.
 */

public class CompleteResultAdapter extends RecyclerView.Adapter<CompleteResultAdapter.CompleteResultHolder> {

    Context mContext;
    ArrayList<UserResultObject> mUserResultList;
    ResultDetailClickListener resultDetailClickListener;

    public CompleteResultAdapter(Context mContext, ArrayList<UserResultObject> mUserResultList) {
        this.mContext = mContext;
        this.mUserResultList = mUserResultList;
    }

    public void setResultDetailClickListener(ResultDetailClickListener resultDetailClickListener) {
        this.resultDetailClickListener = resultDetailClickListener;
    }

    @Override
    public CompleteResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cell_completelist, parent, false);
        CompleteResultHolder holder = new CompleteResultHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CompleteResultHolder holder, int position) {
        final UserResultObject userResultObject = mUserResultList.get(position);
        String[] dateArr = userResultObject.getCreated_at().split(" ");
        String[] takenDate = Tools.getFormattedProfileDate(dateArr[0]);

        holder.mTvDate.setText(takenDate[0]);
        holder.mTvMonth.setText(takenDate[1]);
        holder.mTvYear.setText(takenDate[2]);
        holder.mTvCategoryName.setText(userResultObject.getQuiz_name());

        holder.mLlHolderContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resultDetailClickListener != null) {
                    resultDetailClickListener.onResultDetailClicked(userResultObject);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserResultList.size();
    }

    class CompleteResultHolder extends RecyclerView.ViewHolder {

        LinearLayout mLlHolderContent;
        TextView mTvDate, mTvMonth, mTvYear, mTvCategoryName;

        public CompleteResultHolder(View itemView) {
            super(itemView);
            mLlHolderContent = (LinearLayout) itemView.findViewById(R.id.ll_resultdetail);

            mTvDate = (TextView) itemView.findViewById(R.id.tv_comlist_date);
            mTvMonth = (TextView) itemView.findViewById(R.id.tv_comlist_month);
            mTvYear = (TextView) itemView.findViewById(R.id.tv_comlist_year);
            mTvCategoryName = (TextView) itemView.findViewById(R.id.tv_comlist_categoryname);
        }
    }

    public interface ResultDetailClickListener {
        public void onResultDetailClicked(UserResultObject userResultObject);
    }
}
