package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.smack.questapp.PhotoGalleryActivity;
import com.smack.questapp.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SaraschandraaM on 29/05/16.
 */
public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.ImageViewHolder> implements CompoundButton.OnCheckedChangeListener {

    private SparseBooleanArray mCheckStates;
    ArrayList<Integer> checkedPos = new ArrayList<Integer>();
    int selectCount = 0;
    int screenType;
    boolean isFromAnomaly;
    Context context;
    List<String> imagePaths;

    public ImageGalleryAdapter(Context context, List<String> imagePaths, int screenType) {
        super();
        this.context = context;
        this.imagePaths = imagePaths;
        this.isFromAnomaly = isFromAnomaly;
        this.screenType = screenType;
        mCheckStates = new SparseBooleanArray(imagePaths.size());
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_photogallery, parent, false);
        ImageViewHolder holder = new ImageViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, final int position) {
        holder.imgViewFlag.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        holder.chSelect.setOnCheckedChangeListener(null);
        if (checkedPos.contains(position)) {
            holder.chSelect.setChecked(true);
        } else {
            holder.chSelect.setChecked(false);
        }
        Bitmap bitmap = decodeFile(imagePaths.get(position));
        holder.imgViewFlag.setImageBitmap(bitmap);

        holder.viewLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                String image = imagePaths.get(position);
//                Bundle args = new Bundle();
//                args.putString("imagepath", image);
                toggle(position, holder.chSelect);
            }
        });


        holder.chSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
//                toggle(position, holder.chSelect);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagePaths.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout viewLayout;
        ImageView imgViewFlag;
        CheckBox chSelect;

        public ImageViewHolder(View itemView) {
            super(itemView);
            viewLayout = (RelativeLayout) itemView.findViewById(R.id.root_view);
            imgViewFlag = (ImageView) itemView.findViewById(R.id.imageView1);
            chSelect = (CheckBox) itemView.findViewById(R.id.ch_img_select);
        }
    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String file, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file, options);
    }

    private Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(path), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 200;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE
                    && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(path), null,
                    o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public boolean isChecked(int position) {
        return mCheckStates.get(position, false);
    }

    public void setChecked(int position, boolean isChecked, CheckBox view) {
        mCheckStates.put(position, isChecked);
        String image = imagePaths.get(position);
        if (isChecked) {
            if (screenType == 0) {
                if (selectCount < 1) {
                    PhotoGalleryActivity.selectedPath.add(imagePaths.get(position));
                    view.setChecked(true);
                    mCheckStates.put(position, true);
                    selectCount = selectCount + 1;
                } else {
                    Toast.makeText(context, "You cannot choice multiple choice", Toast.LENGTH_SHORT).show();
                    view.setChecked(false);
                    mCheckStates.put(position, false);
                }
            }
            return;
        } else {
            selectCount = selectCount - 1;
            PhotoGalleryActivity.selectedPath.remove(imagePaths.get(position));
            view.setChecked(false);
            mCheckStates.put(position, false);
        }

    }

    public void toggle(int position, CheckBox view) {
        setChecked(position, !isChecked(position), view);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mCheckStates.put((Integer) buttonView.getTag(), isChecked);
    }
}
