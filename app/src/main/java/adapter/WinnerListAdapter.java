package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smack.questapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import model.WinnerObject;
import utils.Logger;
import utils.Tools;

/**
 * Created by SaraschandraaM on 26/10/16.
 */

public class WinnerListAdapter extends RecyclerView.Adapter<WinnerListAdapter.WinnerListHolder> {

    Context mContext;
    ArrayList<WinnerObject> winnerList;

    WinnerListClickListener winnerListClickListener;

    public WinnerListAdapter(Context mContext, ArrayList<WinnerObject> winnerList) {
        this.mContext = mContext;
        this.winnerList = winnerList;
    }

    public void setWinnerClickListener(WinnerListClickListener winnerListClickListener) {
        this.winnerListClickListener = winnerListClickListener;
    }

    @Override
    public WinnerListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cell_winnerlist, parent, false);
        WinnerListHolder holder = new WinnerListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(WinnerListHolder holder, final int position) {
        WinnerObject winnerObject = winnerList.get(position);
        holder.mTvWinnerName.setText(winnerObject.getUser_name());
        holder.mTvWinnerCollege.setText(winnerObject.getCollege_name());
        Logger.showInfo("WinnerType", winnerObject.getType());
        switch (winnerObject.getType()) {
            case "Week":
                String[] winperiod = Tools.getWinnerPeriod(winnerObject.getPeriod());
                holder.mTvPeriod.setText(winperiod[0]);
                holder.mTvPeriodYear.setText(winperiod[1]);
                break;
        }

        if (Tools.checkIfStringisValid(winnerObject.getProfile_image())) {
            Picasso.with(mContext).load(winnerObject.getProfile_image()).placeholder(R.drawable.img_person).into(holder.mIvWinnerImage);
        }

        holder.mLlWinnerHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (winnerListClickListener != null) {
                    winnerListClickListener.onWinnerListClicked(position, winnerList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return winnerList.size();
    }

    class WinnerListHolder extends RecyclerView.ViewHolder {

        TextView mTvWinnerName, mTvWinnerCollege, mTvPeriod, mTvPeriodYear;
        ImageView mIvWinnerImage;
        LinearLayout mLlWinnerHolder;

        public WinnerListHolder(View itemView) {
            super(itemView);
            mTvWinnerName = (TextView) itemView.findViewById(R.id.tv_winner_name);
            mTvWinnerCollege = (TextView) itemView.findViewById(R.id.tv_winner_college);
            mTvPeriod = (TextView) itemView.findViewById(R.id.tv_winner_period);
            mTvPeriodYear = (TextView) itemView.findViewById(R.id.tv_winner_period_year);

            mIvWinnerImage = (ImageView) itemView.findViewById(R.id.iv_winner_img);

            mLlWinnerHolder = (LinearLayout) itemView.findViewById(R.id.ll_winner_holder);
        }
    }

    public interface WinnerListClickListener {
        public void onWinnerListClicked(int position, WinnerObject winnerObject);
    }
}
