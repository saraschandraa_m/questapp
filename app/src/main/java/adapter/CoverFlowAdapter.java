package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smack.questapp.R;

import java.util.ArrayList;

import model.CarouselData;

public class CoverFlowAdapter extends BaseAdapter {

    private ArrayList<CarouselData> mData = new ArrayList<>(0);
    private Context mContext;

    public CoverFlowAdapter(Context context) {
        mContext = context;
    }

    public void setData(ArrayList<CarouselData> data) {
        mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int pos) {
        return mData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.cell_carrouselitem, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.name);
            viewHolder.image = (ImageView) rowView.findViewById(R.id.image);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

//        holder.image.setImageResource(mData.get(position).imageResId);
        holder.text.setText(mData.get(position).titleResId);

        int imageCategory = mData.get(position).imageResId;
        switch (imageCategory) {
            case 2:
                holder.image.setImageResource(R.drawable.ic_category_sports);
                break;

            case 3:
                holder.image.setImageResource(R.drawable.ic_category_business);
                break;

            case 4:
                holder.image.setImageResource(R.drawable.ic_category_logo);
                break;

            case 5:
                holder.image.setImageResource(R.drawable.ic_category_stocks);
                break;

            case 6:
                holder.image.setImageResource(R.drawable.ic_category_humanresource);
                break;

            case 7:
                holder.image.setImageResource(R.drawable.ic_category_marketing);
                break;

            case 8:
                holder.image.setImageResource(R.drawable.ic_category_finance);
                break;

            case 9:
                holder.image.setImageResource(R.drawable.ic_category_operations);
                break;

            case 10:
                holder.image.setImageResource(R.drawable.ic_category_media);
                break;

            default:
                holder.image.setImageResource(R.drawable.ic_gk);
                break;
        }

        return rowView;
    }


    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }
}
