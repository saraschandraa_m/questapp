package listeners;

/**
 * Created by SaraschandraaM on 09/11/16.
 */

public interface ProfileUpdatedListener {

    public void onProfileUpdated();
}
