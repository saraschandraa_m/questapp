package listeners;

import model.ResponseObject;

/**
 * Created by SaraschandraaM on 22/06/16.
 */
public interface WSListener {

    public void onRequestSent();

    public void onResposneReceived(ResponseObject responseObject);
}
