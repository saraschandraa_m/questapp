package listeners;

/**
 * Created by SaraschandraaM on 22/12/16.
 */

public interface ShareClickListener {

    public void onShareClicked(int shareCode);
}
