package listeners;

/**
 * Created by SaraschandraaM on 16/12/16.
 */

public interface TokenRegisterListener {

    public void onTokenRegistered(String strRegId);
}
