package services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by SaraschandraaM on 28/06/16.
 */
public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("AlarmReceiver", "Class Called");
        Toast.makeText(context, "Time up", Toast.LENGTH_SHORT).show();
    }
}
