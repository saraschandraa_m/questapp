package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 27/10/16.
 */

public class UserResultObject implements Serializable {

    int quiz_id, no_of_questions, duration, pass_percentage, status,
            quiz_result_id, user_id, mark_obtained, percentage_obtained, no_of_question_attended,
            no_of_correct, no_of_wrong;

    String quiz_name, description, created_at, total_time;

    CatchJSONExceptions mCatchJSONExceptions;

    public UserResultObject() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public UserResultObject(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        quiz_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "quiz_id");
        no_of_questions = mCatchJSONExceptions.getIntFromJSON(jsonObject, "no_of_questions");
        duration = mCatchJSONExceptions.getIntFromJSON(jsonObject, "duration");
        pass_percentage = mCatchJSONExceptions.getIntFromJSON(jsonObject, "pass_percentage");
        status = mCatchJSONExceptions.getIntFromJSON(jsonObject, "status");
        quiz_result_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "quiz_result_id");
        user_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "user_id");
        mark_obtained = mCatchJSONExceptions.getIntFromJSON(jsonObject, "mark_obtained");
        percentage_obtained = mCatchJSONExceptions.getIntFromJSON(jsonObject, "percentage_obtained");
        no_of_question_attended = mCatchJSONExceptions.getIntFromJSON(jsonObject, "no_of_question_attended");
        no_of_correct = mCatchJSONExceptions.getIntFromJSON(jsonObject, "no_of_correct");
        no_of_wrong = mCatchJSONExceptions.getIntFromJSON(jsonObject, "no_of_wrong");

        quiz_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "quiz_name");
        description = mCatchJSONExceptions.getStringFromJSON(jsonObject, "description");
        created_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created_at");
        total_time = mCatchJSONExceptions.getStringFromJSON(jsonObject, "total_time");
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public int getNo_of_questions() {
        return no_of_questions;
    }

    public void setNo_of_questions(int no_of_questions) {
        this.no_of_questions = no_of_questions;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPass_percentage() {
        return pass_percentage;
    }

    public void setPass_percentage(int pass_percentage) {
        this.pass_percentage = pass_percentage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getQuiz_result_id() {
        return quiz_result_id;
    }

    public void setQuiz_result_id(int quiz_result_id) {
        this.quiz_result_id = quiz_result_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getMark_obtained() {
        return mark_obtained;
    }

    public void setMark_obtained(int mark_obtained) {
        this.mark_obtained = mark_obtained;
    }

    public int getPercentage_obtained() {
        return percentage_obtained;
    }

    public void setPercentage_obtained(int percentage_obtained) {
        this.percentage_obtained = percentage_obtained;
    }

    public int getNo_of_question_attended() {
        return no_of_question_attended;
    }

    public void setNo_of_question_attended(int no_of_question_attended) {
        this.no_of_question_attended = no_of_question_attended;
    }

    public int getNo_of_correct() {
        return no_of_correct;
    }

    public void setNo_of_correct(int no_of_correct) {
        this.no_of_correct = no_of_correct;
    }

    public int getNo_of_wrong() {
        return no_of_wrong;
    }

    public void setNo_of_wrong(int no_of_wrong) {
        this.no_of_wrong = no_of_wrong;
    }

    public String getQuiz_name() {
        return quiz_name;
    }

    public void setQuiz_name(String quiz_name) {
        this.quiz_name = quiz_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTotal_time() {
        return total_time;
    }

    public void setTotal_time(String total_time) {
        this.total_time = total_time;
    }
}
