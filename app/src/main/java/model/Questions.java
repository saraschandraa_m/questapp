package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 30/07/16.
 */
public class Questions implements Serializable {

    int question_id;
    int quiz_id;
    String question_type;
    String question, question_image;
    String answer;
    int marks;
    String created;
    int status;

    ArrayList<QuestionsOptions> questionsOptionses;

    CatchJSONExceptions mCatchJSONExceptions;

    public Questions() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public Questions(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();
        question_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "question_id");
        quiz_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "quiz_id");
        question_type = mCatchJSONExceptions.getStringFromJSON(jsonObject, "question_type");
        question = mCatchJSONExceptions.getStringFromJSON(jsonObject, "question");
        marks = mCatchJSONExceptions.getIntFromJSON(jsonObject, "marks");
        created = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created");
        status = mCatchJSONExceptions.getIntFromJSON(jsonObject, "status");
        answer = mCatchJSONExceptions.getStringFromJSON(jsonObject, "answer");
        question_image = mCatchJSONExceptions.getStringFromJSON(jsonObject, "question_image");

        questionsOptionses = new ArrayList<>();
        JSONArray jsonArray = mCatchJSONExceptions.getArrayFromJSON(jsonObject, "question_options");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jObj = (JSONObject) mCatchJSONExceptions.getJSONObjectFromJSONArray(jsonArray, i);
            QuestionsOptions options = new QuestionsOptions(jObj);
            questionsOptionses.add(options);
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean isEqual = false;

        if (object != null && object instanceof Questions) {
            isEqual = (this.question.equals(((Questions) object).question));
        }

        return isEqual;
    }

    @Override
    public int hashCode() {
        return this.hashCode();
    }

    public ArrayList<QuestionsOptions> getQuestionsOptionses() {
        return questionsOptionses;
    }

    public void setQuestionsOptionses(ArrayList<QuestionsOptions> questionsOptionses) {
        this.questionsOptionses = questionsOptionses;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion_image() {
        return question_image;
    }

    public void setQuestion_image(String question_image) {
        this.question_image = question_image;
    }
}
