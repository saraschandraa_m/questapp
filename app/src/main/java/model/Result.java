package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 10/09/16.
 */
public class Result implements Serializable {

    String error;
    String created_at;
    String user_id;
    String result_status;
    String no_of_question_attended;
    String no_of_correct;
    String no_of_wrong;
    String mark_obtained;
    String total_time;
    int status;

    CatchJSONExceptions mCatchJSONExceptions;

    public Result() {

    }

    public Result(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();
        error = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");
        created_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created_at");
        user_id = mCatchJSONExceptions.getStringFromJSON(jsonObject, "user_id");
        result_status = mCatchJSONExceptions.getStringFromJSON(jsonObject, "result_status");
        no_of_question_attended = mCatchJSONExceptions.getStringFromJSON(jsonObject, "no_of_question_attended");
        no_of_correct = mCatchJSONExceptions.getStringFromJSON(jsonObject, "no_of_correct");
        no_of_wrong = mCatchJSONExceptions.getStringFromJSON(jsonObject, "no_of_wrong");
        mark_obtained = mCatchJSONExceptions.getStringFromJSON(jsonObject, "mark_obtained");
        total_time = mCatchJSONExceptions.getStringFromJSON(jsonObject, "total_time");
        status = mCatchJSONExceptions.getIntFromJSON(jsonObject, "status");
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getResult_status() {
        return result_status;
    }

    public void setResult_status(String result_status) {
        this.result_status = result_status;
    }

    public String getNo_of_question_attended() {
        return no_of_question_attended;
    }

    public void setNo_of_question_attended(String no_of_question_attended) {
        this.no_of_question_attended = no_of_question_attended;
    }

    public String getNo_of_correct() {
        return no_of_correct;
    }

    public void setNo_of_correct(String no_of_correct) {
        this.no_of_correct = no_of_correct;
    }

    public String getNo_of_wrong() {
        return no_of_wrong;
    }

    public void setNo_of_wrong(String no_of_wrong) {
        this.no_of_wrong = no_of_wrong;
    }

    public String getMark_obtained() {
        return mark_obtained;
    }

    public void setMark_obtained(String mark_obtained) {
        this.mark_obtained = mark_obtained;
    }

    public String getTotal_time() {
        return total_time;
    }

    public void setTotal_time(String total_time) {
        this.total_time = total_time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
