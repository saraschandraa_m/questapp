package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 26/10/16.
 */

public class WinnerObject implements Serializable {

    String created_at, type, user_name, college_name, period, remarks, gift, profile_image;
    int user_winner_id, user_id, quiz_id, quiz_result_id, status;

    CatchJSONExceptions mCatchJSONExceptions;

    public WinnerObject(){
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public WinnerObject(JSONObject jsonObject){
        mCatchJSONExceptions = new CatchJSONExceptions();

        created_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created_at");
        type = mCatchJSONExceptions.getStringFromJSON(jsonObject, "type");
        user_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "user_name");
        college_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "college_name");
        period = mCatchJSONExceptions.getStringFromJSON(jsonObject, "period");
        remarks = mCatchJSONExceptions.getStringFromJSON(jsonObject, "remarks");
        gift = mCatchJSONExceptions.getStringFromJSON(jsonObject, "gift");
        profile_image = mCatchJSONExceptions.getStringFromJSON(jsonObject, "profile_image");

        user_winner_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "user_winner_id");
        user_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "user_id");
        quiz_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "quiz_id");
        quiz_result_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "quiz_result_id");
        status = mCatchJSONExceptions.getIntFromJSON(jsonObject, "status");
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public int getUser_winner_id() {
        return user_winner_id;
    }

    public void setUser_winner_id(int user_winner_id) {
        this.user_winner_id = user_winner_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public int getQuiz_result_id() {
        return quiz_result_id;
    }

    public void setQuiz_result_id(int quiz_result_id) {
        this.quiz_result_id = quiz_result_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
