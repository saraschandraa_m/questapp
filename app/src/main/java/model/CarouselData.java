package model;

/**
 * Created by SaraschandraaM on 22/09/16.
 */

public class CarouselData {

    public int imageResId;
    public String titleResId;

    public CarouselData(int imageResId, String titleResId) {
        this.imageResId = imageResId;
        this.titleResId = titleResId;
    }
}
