package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 26/10/16.
 */

public class UserRanking implements Serializable {

    String error;
    int total_no_quiz, win_percentage, no_of_times_won, personal_rating;

    CatchJSONExceptions mCatchJSONExceptions;

    public UserRanking() {

    }

    public UserRanking(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        error = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");

        total_no_quiz = mCatchJSONExceptions.getIntFromJSON(jsonObject, "total_no_quiz");
        win_percentage = mCatchJSONExceptions.getIntFromJSON(jsonObject, "win_percentage");
        no_of_times_won = mCatchJSONExceptions.getIntFromJSON(jsonObject, "no_of_times_won");
        personal_rating = mCatchJSONExceptions.getIntFromJSON(jsonObject, "personal_rating");
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getTotal_no_quiz() {
        return total_no_quiz;
    }

    public void setTotal_no_quiz(int total_no_quiz) {
        this.total_no_quiz = total_no_quiz;
    }

    public int getWin_percentage() {
        return win_percentage;
    }

    public void setWin_percentage(int win_percentage) {
        this.win_percentage = win_percentage;
    }

    public int getNo_of_times_won() {
        return no_of_times_won;
    }

    public void setNo_of_times_won(int no_of_times_won) {
        this.no_of_times_won = no_of_times_won;
    }

    public int getPersonal_rating() {
        return personal_rating;
    }

    public void setPersonal_rating(int personal_rating) {
        this.personal_rating = personal_rating;
    }
}
