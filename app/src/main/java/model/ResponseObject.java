package model;

import java.io.Serializable;

/**
 * Created by SaraschandraaM on 06/06/16.
 */
public class ResponseObject implements Serializable {

    int responseCode;
    String response;
    String headerToken;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getHeaderToken() {
        return headerToken;
    }

    public void setHeaderToken(String headerToken) {
        this.headerToken = headerToken;
    }
}
