package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 28/06/16.
 */
public class CategoryList implements Serializable {

    ArrayList<CategoryItem> mCategories;
    String errorCode;

    CatchJSONExceptions mCatchJSONExceptions;

    public CategoryList() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public CategoryList(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        errorCode = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");


        mCategories = new ArrayList<>();
        JSONArray jArray = mCatchJSONExceptions.getArrayFromJSON(jsonObject, "category");
        for (int i = 0; i < jArray.length(); i++) {
            JSONObject jObj = mCatchJSONExceptions.getJSONObjectFromJSONArray(jArray, i);
            CategoryItem data = new CategoryItem(jObj);
            mCategories.add(data);
        }
    }

    public ArrayList<CategoryItem> getmCategories() {
        return mCategories;
    }

    public void setmCategories(ArrayList<CategoryItem> mCategories) {
        this.mCategories = mCategories;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
