package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 24/06/16.
 */
public class CategoryItem implements Serializable {

    String quiz_name;
    String description;
    String start_date;
    String end_date;
    String ip_address;
    String created_at;

    int no_of_questions;
    int quiz_id;
    int duration;
    int maximum_attempt_per_user;
    int pass_percentage;
    int view_answer;
    int question_selection;
    int status;

    CatchJSONExceptions mCatchJSONExceptions;

    public CategoryItem() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public CategoryItem(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();
        quiz_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "quiz_id");
        quiz_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "quiz_name");
        description = mCatchJSONExceptions.getStringFromJSON(jsonObject, "description");
        start_date = mCatchJSONExceptions.getStringFromJSON(jsonObject, "start_date");
        end_date = mCatchJSONExceptions.getStringFromJSON(jsonObject, "end_date");
        no_of_questions = mCatchJSONExceptions.getIntFromJSON(jsonObject, "no_of_questions");
        ip_address = mCatchJSONExceptions.getStringFromJSON(jsonObject, "ip_address");
        duration = mCatchJSONExceptions.getIntFromJSON(jsonObject, "duration");
        maximum_attempt_per_user = mCatchJSONExceptions.getIntFromJSON(jsonObject, "maximum_attempt_per_user");
        pass_percentage = mCatchJSONExceptions.getIntFromJSON(jsonObject, "pass_percentage");
        view_answer = mCatchJSONExceptions.getIntFromJSON(jsonObject, "view_answer");
        question_selection = mCatchJSONExceptions.getIntFromJSON(jsonObject, "question_selection");
        created_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created_at");
        status = mCatchJSONExceptions.getIntFromJSON(jsonObject, "status");
    }

    public String getQuiz_name() {
        return quiz_name;
    }

    public void setQuiz_name(String quiz_name) {
        this.quiz_name = quiz_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getNo_of_questions() {
        return no_of_questions;
    }

    public void setNo_of_questions(int no_of_questions) {
        this.no_of_questions = no_of_questions;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getMaximum_attempt_per_user() {
        return maximum_attempt_per_user;
    }

    public void setMaximum_attempt_per_user(int maximum_attempt_per_user) {
        this.maximum_attempt_per_user = maximum_attempt_per_user;
    }

    public int getPass_percentage() {
        return pass_percentage;
    }

    public void setPass_percentage(int pass_percentage) {
        this.pass_percentage = pass_percentage;
    }

    public int getView_answer() {
        return view_answer;
    }

    public void setView_answer(int view_answer) {
        this.view_answer = view_answer;
    }

    public int getQuestion_selection() {
        return question_selection;
    }

    public void setQuestion_selection(int question_selection) {
        this.question_selection = question_selection;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
