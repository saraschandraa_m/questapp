package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 27/10/16.
 */

public class UserResultList implements Serializable {


    String error;
    ArrayList<UserResultObject> userResultList;

    CatchJSONExceptions mCatchJSONExceptions;

    public UserResultList() {

    }

    public UserResultList(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        error = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");

        userResultList = new ArrayList<>();
        JSONArray userResultArray = mCatchJSONExceptions.getArrayFromJSON(jsonObject, "results");
        if (userResultArray != null) {
            for (int i = 0; i < userResultArray.length(); i++) {
                JSONObject userResultObj = mCatchJSONExceptions.getJSONObjectFromJSONArray(userResultArray, i);
                UserResultObject userResultObject = new UserResultObject(userResultObj);
                userResultList.add(userResultObject);
            }
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<UserResultObject> getUserResultList() {
        return userResultList;
    }

    public void setUserResultList(ArrayList<UserResultObject> userResultList) {
        this.userResultList = userResultList;
    }
}
