package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 22/06/16.
 */
public class UserInfo implements Serializable {

    String error, user_id, created_at, modified_at, gcmregid, email, password, first_name, last_name, contact_no,
            college_name, dob, gender, is_from_faceoobk, facebook_id, available_credits, is_admin, is_active, profile_image, location;

    CatchJSONExceptions mCatchJSONExceptions;

    public UserInfo() {

    }

    public UserInfo(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        error = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");
        user_id = mCatchJSONExceptions.getStringFromJSON(jsonObject, "user_id");
        created_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created_at");
        modified_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "modified_at");
        gcmregid = mCatchJSONExceptions.getStringFromJSON(jsonObject, "gcmregid");
        email = mCatchJSONExceptions.getStringFromJSON(jsonObject, "email");
        password = mCatchJSONExceptions.getStringFromJSON(jsonObject, "password");
        first_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "first_name");
        last_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "last_name");
        contact_no = mCatchJSONExceptions.getStringFromJSON(jsonObject, "contact_no");
        college_name = mCatchJSONExceptions.getStringFromJSON(jsonObject, "college_name");
        dob = mCatchJSONExceptions.getStringFromJSON(jsonObject, "dob");
        gender = mCatchJSONExceptions.getStringFromJSON(jsonObject, "gender");
        is_from_faceoobk = mCatchJSONExceptions.getStringFromJSON(jsonObject, "is_from_faceoobk");
        facebook_id = mCatchJSONExceptions.getStringFromJSON(jsonObject, "facebook_id");
        available_credits = mCatchJSONExceptions.getStringFromJSON(jsonObject, "available_credits");
        is_admin = mCatchJSONExceptions.getStringFromJSON(jsonObject, "is_admin");
        is_active = mCatchJSONExceptions.getStringFromJSON(jsonObject, "is_active");
        profile_image = mCatchJSONExceptions.getStringFromJSON(jsonObject, "profile_image");
        location = mCatchJSONExceptions.getStringFromJSON(jsonObject, "location");
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public String getGcmregid() {
        return gcmregid;
    }

    public void setGcmregid(String gcmregid) {
        this.gcmregid = gcmregid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIs_from_faceoobk() {
        return is_from_faceoobk;
    }

    public void setIs_from_faceoobk(String is_from_faceoobk) {
        this.is_from_faceoobk = is_from_faceoobk;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getAvailable_credits() {
        return available_credits;
    }

    public void setAvailable_credits(String available_credits) {
        this.available_credits = available_credits;
    }

    public String getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(String is_admin) {
        this.is_admin = is_admin;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
