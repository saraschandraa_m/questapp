package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 26/10/16.
 */

public class WinnerList implements Serializable {

    String error;
    ArrayList<WinnerObject> winnerList;

    CatchJSONExceptions mCatchJSONExceptions;

    public WinnerList() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public WinnerList(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        error = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");

        winnerList = new ArrayList<>();
        JSONArray winnerArray = mCatchJSONExceptions.getArrayFromJSON(jsonObject, "winner_list");
        for (int i = 0; i < winnerArray.length(); i++) {
            JSONObject winnerObj = mCatchJSONExceptions.getJSONObjectFromJSONArray(winnerArray, i);
            WinnerObject winnerObject = new WinnerObject(winnerObj);
            winnerList.add(winnerObject);
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<WinnerObject> getWinnerList() {
        return winnerList;
    }

    public void setWinnerList(ArrayList<WinnerObject> winnerList) {
        this.winnerList = winnerList;
    }
}
