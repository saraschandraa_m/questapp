package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 31/07/16.
 */
public class QuestionList implements Serializable {

    String error;
    ArrayList<Questions> questionList;

    CatchJSONExceptions mCatchJSONExceptions;

    public QuestionList() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public QuestionList(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();
        error = mCatchJSONExceptions.getStringFromJSON(jsonObject, "error");

        questionList = new ArrayList<>();
        JSONArray questionsArray = mCatchJSONExceptions.getArrayFromJSON(jsonObject, "questions");
        if (questionsArray != null && questionsArray.length() > 0) {
            for (int i = 0; i < questionsArray.length(); i++) {
                JSONObject jObj = (JSONObject) mCatchJSONExceptions.getJSONObjectFromJSONArray(questionsArray, i);
                Questions question = new Questions(jObj);
                questionList.add(question);
            }
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<Questions> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(ArrayList<Questions> questionList) {
        this.questionList = questionList;
    }
}
