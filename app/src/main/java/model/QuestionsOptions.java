package model;

import org.json.JSONObject;

import java.io.Serializable;

import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 30/07/16.
 */
public class QuestionsOptions implements Serializable {

    int question_option_id;
    int question_id;
    String question_option;
    String question_option_match;
    String created_at;
    int marks;
    int status;

    CatchJSONExceptions mCatchJSONExceptions;

    public QuestionsOptions() {
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public QuestionsOptions(JSONObject jsonObject) {
        mCatchJSONExceptions = new CatchJSONExceptions();

        question_option_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "question_option_id");
        question_id = mCatchJSONExceptions.getIntFromJSON(jsonObject, "question_id");
        marks = mCatchJSONExceptions.getIntFromJSON(jsonObject, "marks");
        status = mCatchJSONExceptions.getIntFromJSON(jsonObject, "status");
        question_option = mCatchJSONExceptions.getStringFromJSON(jsonObject, "question_option");
        question_option_match = mCatchJSONExceptions.getStringFromJSON(jsonObject, "question_option_match");
        created_at = mCatchJSONExceptions.getStringFromJSON(jsonObject, "created_at");
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getQuestion_option_match() {
        return question_option_match;
    }

    public void setQuestion_option_match(String question_option_match) {
        this.question_option_match = question_option_match;
    }

    public String getQuestion_option() {
        return question_option;
    }

    public void setQuestion_option(String question_option) {
        this.question_option = question_option;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getQuestion_option_id() {
        return question_option_id;
    }

    public void setQuestion_option_id(int question_option_id) {
        this.question_option_id = question_option_id;
    }
}
