package webservices;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import model.ResponseObject;

/**
 * Created by KMac on 6/9/16.
 */
public class ServiceResponse {

    public ResponseObject getResponse(String BaseUrl, String strEncBase64, String strEncMD5) {
        ResponseObject responseObject = new ResponseObject();
        String strJson = "";
        try {
            String url = BaseUrl + "dothis=" + strEncBase64 + "&andthis=" + strEncMD5;

            Log.d("Webservices", "Post URL : " + url);

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            int responseCode = con.getResponseCode();
//            Log.d("Webservices", "\nSending 'GET' request to URL : " + url);
//            Log.d("Webservices", "Response Code : " + responseCode);
            responseObject.setResponseCode(responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            if (responseCode == 200) {
//                Log.i("WS Response", response.toString());
                strJson = response.toString();
                strJson = strJson.replace("ï»¿", "");
                responseObject.setResponse(strJson);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseObject;
    }
}
