package aynctask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.json.JSONObject;

import listeners.WSListener;
import model.ResponseObject;
import utils.CatchJSONExceptions;
import utils.Urls;

/**
 * Created by SaraschandraaM on 30/07/16.
 */
public class WS_Register {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;

    public interface RegistrationListener {
        public void onRegistrationDone(ResponseObject responseObject);
    }

    public WS_Register(Context mContext) {
        this.mContext = mContext;
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public void doRegister(String param, final RegistrationListener registrationListener) {
        new WebServiceTask(mContext, param, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                registrationListener.onRegistrationDone(responseObject);
            }
        }).execute();
    }
}
