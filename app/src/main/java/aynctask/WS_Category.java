package aynctask;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import listeners.WSListener;
import model.CategoryItem;
import model.CategoryList;
import model.ResponseObject;
import utils.CatchJSONExceptions;
import utils.Logger;
import utils.Urls;

/**
 * Created by SaraschandraaM on 28/06/16.
 */
public class WS_Category {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;
    String TAG = "GetCategory";

    public interface CategoryListener {
        public void onCategoryReceived(boolean isSuccess, ArrayList<CategoryItem> categoryList);
    }

    public WS_Category(Context mContext) {
        this.mContext = mContext;
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public void doGetCategory(String userID, final CategoryListener categoryListener) {
        String params = Urls.CATEGORY + userID;
        new WebServiceTask(mContext, params, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, responseObject.getResponse());
                    JSONObject response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    CategoryList categoryList = new CategoryList(response);
                    ArrayList<CategoryItem> mCategories = new ArrayList<CategoryItem>();
                    mCategories = categoryList.getmCategories();
                    if (categoryList.getErrorCode().equals("OWCE00")) {
                        categoryListener.onCategoryReceived(true, mCategories);
                    } else {
                        categoryListener.onCategoryReceived(false, null);
                    }
                } else {
                    categoryListener.onCategoryReceived(false, null);
                }
            }
        }).execute();
    }
}
