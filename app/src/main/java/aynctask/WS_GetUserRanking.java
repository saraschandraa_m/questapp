package aynctask;

import android.content.Context;

import org.json.JSONObject;

import listeners.WSListener;
import model.ResponseObject;
import model.UserRanking;
import utils.CatchJSONExceptions;
import utils.Logger;

/**
 * Created by SaraschandraaM on 29/09/16.
 */

public class WS_GetUserRanking {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;
    String params, TAG = "GetUserRanking";

    public interface UserRankingReceivedListener {
        public void onUserRankingReceived(boolean isSuccess, UserRanking userRanking);
    }

    public WS_GetUserRanking(Context mContext, String params) {
        this.mContext = mContext;
        this.params = params;
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public void execute(final UserRankingReceivedListener userRankingReceivedListener) {
        new WebServiceTask(mContext, params, new WSListener() {
            @Override
            public void onRequestSent() {
                Logger.showInfo(TAG, "Request Sent");
            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                Logger.showInfo(TAG, "Response Received, Code=" + responseObject.getResponseCode());
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, responseObject.getResponse());
                    JSONObject userrankingObj = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    UserRanking userRanking = new UserRanking(userrankingObj);
                    if (userRanking.getError().equals("OWCE00")) {
                        userRankingReceivedListener.onUserRankingReceived(true, userRanking);
                    } else {
                        userRankingReceivedListener.onUserRankingReceived(false, null);
                    }
                } else {
                    userRankingReceivedListener.onUserRankingReceived(false, null);
                }
            }
        }).execute();
    }

}
