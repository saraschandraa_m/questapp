package aynctask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.smack.questapp.R;
import com.smack.questapp.RegisterActivity;

import utils.EncryptionClass;
import utils.Singleton;
import utils.Urls;
import webservices.ServiceResponse;

/**
 * Created by KMac on 6/9/16.
 */
public class RegisterTask extends AsyncTask<Void, Void, Void> {
    private RegisterActivity mActivity;
    private String strGcmID;
    private String strFirstName;
    private String strLastName;
    private String strPassword;
    private String strEmailId;
    private String strLocation;
    private String strCollegeName;
    private String strPhoneNumber;
    private String strDOB;
    private String strGender;

    public RegisterTask(RegisterActivity mActivity, String strGcmID, String strFirstName, String strLastName, String strPassword, String strEmailId,
                        String strLocation, String strCollegeName, String strPhoneNumber, String strDOB, String strGender) {
        this.mActivity = mActivity;
        this.strGcmID = strGcmID;
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strPassword = strPassword;
        this.strEmailId = strEmailId;
        this.strLocation = strLocation;
        this.strCollegeName = strCollegeName;
        this.strPhoneNumber = strPhoneNumber;
        this.strDOB = strDOB;
        this.strGender = strGender;
    }

    private ProgressDialog mProgressDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = ProgressDialog.show(mActivity, "", mActivity.getResources().getString(R.string.please_wait));

    }

    @Override
    protected Void doInBackground(Void... params) {

        EncryptionClass encryptionClass = Singleton.getEncryption();
        String strBase64 = Urls.REGISTER_1_GCM_ID + strGcmID + Urls.REGISTER_2_FIRST_NAME + strFirstName
                + Urls.REGISTER_3_LAST_NAME + strLastName + Urls.REGISTER_4_PASSWORD + strPassword + Urls.REGISTER_5_EMAIL + strEmailId
                + Urls.REGISTER_6_LOCATION + strLocation + Urls.REGISTER_7_COLLEGE_NAME + strCollegeName
                + Urls.REGISTER_8_PHONE_NUMBER + strPhoneNumber + Urls.REGISTER_9_DATE_OF_BIRTH + strDOB
                + Urls.REGISTER_10_GENDER + strGender;

        String strMD5 = strBase64 + Urls.PASSCODE;

        Log.d("RegisterActivity", "ssuab base64: " + strBase64);
        Log.d("RegisterActivity", "ssuab md5: " + strMD5);

        String strEncBase64 = encryptionClass.get_Convert_Base64(strBase64);
        String strEncMD5 = encryptionClass.get_Convert_MD5key(strMD5);

        Log.d("RegisterActivity", "ssuab strEncBase64: " + strEncBase64);
        Log.d("RegisterActivity", "ssuab strEncMD5: " + strEncMD5);

//        ServiceResponse serviceResponse = Singleton.getWebservices();
//        String response = serviceResponse.getResponse(Urls.BASE_URL, strEncBase64, strEncMD5);

//        Log.d("RegisterActivity", "ssuab response: " + response);


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
