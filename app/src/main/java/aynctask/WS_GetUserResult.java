package aynctask;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import listeners.WSListener;
import model.ResponseObject;
import model.UserResultList;
import model.UserResultObject;
import utils.CatchJSONExceptions;
import utils.Logger;

/**
 * Created by SaraschandraaM on 26/10/16.
 */

public class WS_GetUserResult {

    Context mContext;
    String param, TAG = "GetUserResult";
    CatchJSONExceptions mCatchJSONExceptions;

    public interface UserResultListener {
        public void onUserResultReceived(boolean isSuccess, ArrayList<UserResultObject> userResultList);
    }

    public WS_GetUserResult(Context mContext, String param) {
        this.mContext = mContext;
        this.param = param;
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public void execute(final UserResultListener userResultListener) {
        new WebServiceTask(mContext, param, new WSListener() {
            @Override
            public void onRequestSent() {
                Logger.showInfo(TAG, "Request Sent");
            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                Logger.showInfo(TAG, "Response Received, ResponseCode: " + responseObject.getResponseCode());
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, responseObject.getResponse());
                    JSONObject userResultObj = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    UserResultList userResultList = new UserResultList(userResultObj);
                    if (userResultList.getError().equals("OWCE00")) {
                        userResultListener.onUserResultReceived(true, userResultList.getUserResultList());
                    } else {
                        userResultListener.onUserResultReceived(false, null);
                    }
                } else {
                    userResultListener.onUserResultReceived(false, null);
                }
            }
        }).execute();
    }
}
