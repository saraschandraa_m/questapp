package aynctask;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import listeners.WSListener;
import model.QuestionList;
import model.Questions;
import model.ResponseObject;
import utils.CatchJSONExceptions;
import utils.Logger;

/**
 * Created by SaraschandraaM on 31/07/16.
 */
public class WS_Questions {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;
    ArrayList<Questions> questionList;
    String TAG = "QuestionList";

    public interface QuestionsListener {
        public void onQuestionsReceived(boolean isSuccess, boolean isQuestionsAvailable, ArrayList<Questions> questionList);
    }

    public WS_Questions(Context mContext) {
        this.mContext = mContext;
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public void doGetQuestions(String param, final QuestionsListener questionsListener) {
        new WebServiceTask(mContext, param, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, responseObject.getResponse());
                    JSONObject response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    QuestionList questionsObject = new QuestionList(response);
                    if (questionsObject.getError().equals("OWCE00")) {
                        questionList = new ArrayList<Questions>();
                        questionList = questionsObject.getQuestionList();
                        questionsListener.onQuestionsReceived(true, true, questionList);
                    } else if (questionsObject.getError().equals("OWCE02")) {
                        questionsListener.onQuestionsReceived(true, false, questionList);
                    }

                } else {
                    questionsListener.onQuestionsReceived(false, false, null);
                }
            }
        }).execute();
    }
}
