package aynctask;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.smack.questapp.ApplicationEx;

import org.json.JSONObject;

import listeners.WSListener;
import model.ResponseObject;
import model.UserInfo;
import utils.CatchJSONExceptions;
import utils.Constants;
import utils.Logger;
import utils.Urls;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 23/10/16.
 */

public class WS_UpdateUserProfile {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;
    UserInfo mChangedInfo;
    String param, TAG = "Update_UserProfile";
    SharedPreferences mLoginPref;
    SharedPreferences.Editor mLoginEditor;
    WSLoadingDialog wsLoadingDialog;

    public interface ProfileUpdateListener {
        public void onProfileUpdated(boolean isSuccess, UserInfo userInfo);
    }

    public WS_UpdateUserProfile(Context mContext, UserInfo mChangedInfo) {
        this.mContext = mContext;
        this.mChangedInfo = mChangedInfo;
        mCatchJSONExceptions = new CatchJSONExceptions();
        mLoginPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        mLoginEditor = mLoginPref.edit();
        wsLoadingDialog = new WSLoadingDialog(mContext);
    }

    public void execute(final ProfileUpdateListener profileUpdateListener) {
        param = Urls.UPDATE_PROFILE + mChangedInfo.getUser_id() +
                Urls.UPDATE_PROFILE_FIRSTNAME + mChangedInfo.getFirst_name() +
                Urls.UPDATE_PROFILE_LASTNAME + mChangedInfo.getLast_name() +
                Urls.UPDATE_PROFILE_CONTACTNO + mChangedInfo.getContact_no() +
                Urls.UPDATE_PROFILE_COLLEGENAME + mChangedInfo.getCollege_name() +
                Urls.UPDATE_PROFILE_GENDER + mChangedInfo.getGender() +
                Urls.UPDATE_PROFILE_DOB + mChangedInfo.getDob() +
                Urls.UPDATE_PROFILE_LOCATION + mChangedInfo.getLocation();

        new WebServiceTask(mContext, param, new WSListener() {
            @Override
            public void onRequestSent() {
                Logger.showInfo(TAG, "Request Sent");
            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                Logger.showInfo(TAG, "Response Received, Code =" + responseObject.getResponseCode());
                JSONObject response = new JSONObject();
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, "Response = " + responseObject.getResponse());
                    response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    UserInfo updatedInfo = new UserInfo(response);
                    if (updatedInfo.getError().equals("OWCE00")) {
                        profileUpdateListener.onProfileUpdated(true, updatedInfo);
                        mLoginEditor.putString(Constants.PREF_USERDETAILS, responseObject.getResponse());
                        mLoginEditor.commit();
                        ApplicationEx.getAppInstance().setmUserInfo(updatedInfo);
                    } else {
                        profileUpdateListener.onProfileUpdated(false, null);
                    }
                } else {
                    profileUpdateListener.onProfileUpdated(false, null);
                }
            }
        }).execute();
    }

}
