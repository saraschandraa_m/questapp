package aynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import listeners.WSListener;
import model.ResponseObject;
import utils.EncryptionClass;
import utils.Singleton;
import utils.Urls;
import webservices.ServiceResponse;

/**
 * Created by SaraschandraaM on 22/06/16.
 */
public class WebServiceTask extends AsyncTask<ResponseObject, Void, ResponseObject> {

    Context mContext;
    String params;
    WSListener wsListener;

    public WebServiceTask(Context mContext, String params, WSListener wsListener) {
        this.mContext = mContext;
        this.params = params;
        this.wsListener = wsListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (wsListener != null) {
            wsListener.onRequestSent();
        }
    }

    @Override
    protected ResponseObject doInBackground(ResponseObject... responseObjects) {
        ResponseObject responseObject = new ResponseObject();

        EncryptionClass encryptionClass = Singleton.getEncryption();

        String strMD5 = params + Urls.PASSCODE;

//        Log.d("WSParam", params);
//        Log.d("WSMD5", strMD5);

        String strEncBase64 = encryptionClass.get_Convert_Base64(params);
        String strEncMD5 = encryptionClass.get_Convert_MD5key(strMD5);

//        Log.d("WSParam Encrypt", strEncBase64);
//        Log.d("WSMD5 Encrypt", strEncMD5);

        ServiceResponse serviceResponse = Singleton.getWebservices();
        responseObject = serviceResponse.getResponse(Urls.BASE_URL, strEncBase64, strEncMD5);

//        Log.d("Response", responseObject.getResponse());

        return responseObject;
    }

    @Override
    protected void onPostExecute(ResponseObject responseObject) {
        super.onPostExecute(responseObject);
        if (wsListener != null) {
            wsListener.onResposneReceived(responseObject);
        }
    }
}
