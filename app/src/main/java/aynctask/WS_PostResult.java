package aynctask;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

import listeners.WSListener;
import model.QuestionList;
import model.Questions;
import model.ResponseObject;
import model.Result;
import utils.CatchJSONExceptions;

/**
 * Created by SaraschandraaM on 10/09/16.
 */
public class WS_PostResult {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;

    public interface PostResultListener {
        public void onResultsPosted(boolean isSuccess, Result result);
    }

    public WS_PostResult(Context mContext) {
        this.mContext = mContext;
        mCatchJSONExceptions = new CatchJSONExceptions();
    }

    public void doSubmitResults(String param, final PostResultListener postResultListener) {
        new WebServiceTask(mContext, param, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                if (responseObject.getResponseCode() == 200) {
                    Log.i("Response:", responseObject.getResponse());
                    JSONObject response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    Result resultObject = new Result(response);
                    postResultListener.onResultsPosted(true, resultObject);
                } else {
                    postResultListener.onResultsPosted(false, null);
                }
            }
        }).execute();
    }
}
