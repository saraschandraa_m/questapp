package aynctask;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import listeners.WSListener;
import model.ResponseObject;
import model.WinnerList;
import model.WinnerObject;
import utils.CatchJSONExceptions;
import utils.Logger;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 28/09/16.
 */

public class WS_WinnerList {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;
    String params, TAG = "WinnerList";
    WSLoadingDialog wsLoadingDialog;

    public interface WinnerListReceivedListener {
        void onWinnerListReceived(boolean isSuccess, ArrayList<WinnerObject> winnerList);
    }

    public WS_WinnerList(Context mContext, String params) {
        this.mContext = mContext;
        this.params = params;
        mCatchJSONExceptions = new CatchJSONExceptions();
        wsLoadingDialog = new WSLoadingDialog(mContext);
    }

    public void execute(final WinnerListReceivedListener winnerListReceivedListener) {
        new WebServiceTask(mContext, params, new WSListener() {
            @Override
            public void onRequestSent() {
                Logger.showInfo(TAG, "Request Sent");
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                Logger.showInfo(TAG, "Response Received,  ResponseCode: " + responseObject.getResponseCode());
                wsLoadingDialog.hideWSLoadingDialog();
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, responseObject.getResponse());
                    JSONObject responseObj = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    WinnerList winnerList = new WinnerList(responseObj);
                    if (winnerList.getError().equals("OWCE00")) {
                        winnerListReceivedListener.onWinnerListReceived(true, winnerList.getWinnerList());
                    } else {
                        winnerListReceivedListener.onWinnerListReceived(false, null);
                    }
                } else {
                    winnerListReceivedListener.onWinnerListReceived(false, null);
                }
            }
        }).execute();
    }
}
