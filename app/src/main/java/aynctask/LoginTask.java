package aynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.smack.questapp.R;

import utils.EncryptionClass;
import utils.Singleton;
import utils.Urls;
import webservices.ServiceResponse;

/**
 * Created by KMac on 6/9/16.
 */
public class LoginTask extends AsyncTask<Void, Void, Void> {
    private Context mContext;
    private String mStrEmailId;
    private String mStrPassword;
    private String mStrGcmId;
    private ProgressDialog mProgressDialog;

    public LoginTask(Context mContext, String mStrEmailId, String mStrPassword, String mStrGcmId) {
        this.mContext = mContext;
        this.mStrEmailId = mStrEmailId;
        this.mStrPassword = mStrPassword;
        this.mStrGcmId = mStrGcmId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = ProgressDialog.show(mContext,"",mContext.getResources().getString(R.string.please_wait));
    }

    @Override
    protected Void doInBackground(Void... params) {

        EncryptionClass encryptionClass = Singleton.getEncryption();
        String strBase64 = Urls.LOGIN_1_EMAIL_ID+mStrEmailId+Urls.LOGIN_2_PASSWORD+mStrPassword+Urls.LOGIN_3_GCM_ID+mStrGcmId;
        String strMD5 = strBase64+Urls.PASSCODE;

        Log.d("LoginTask","ssuab strBase64: "+strBase64);
        Log.d("LoginTask","ssuab strMD5: "+strMD5);

        String strEncBase64 = encryptionClass.get_Convert_Base64(strBase64);
        String strEncMD5 = encryptionClass.get_Convert_MD5key(strMD5);

        Log.d("LoginTask","ssuab strEncBase64: "+strEncBase64);
        Log.d("LoginTask","ssuab strEncMD5: "+strEncMD5);

        ServiceResponse serviceResponse = Singleton.getWebservices();
//        String response = serviceResponse.getResponse(Urls.BASE_URL, strEncBase64, strEncMD5);
//
//        Log.d("RegisterActivity","ssuab response: "+response);



        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
