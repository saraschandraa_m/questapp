package aynctask;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.smack.questapp.ApplicationEx;

import org.json.JSONObject;

import listeners.WSListener;
import model.ResponseObject;
import model.UserInfo;
import utils.CatchJSONExceptions;
import utils.Constants;
import utils.Logger;
import utils.Urls;

/**
 * Created by SaraschandraaM on 28/06/16.
 */
public class WS_Login {

    Context mContext;
    CatchJSONExceptions mCatchJSONExceptions;
    SharedPreferences mLoginPref;
    SharedPreferences.Editor mLoginEditor;
    String TAG = "LOGIN";

    public interface LoginListener {
        public void onLoginCompleted(boolean isSucess, UserInfo mResponse);
    }

    public WS_Login(Context mContext) {
        this.mContext = mContext;
        mCatchJSONExceptions = new CatchJSONExceptions();
        mLoginPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        mLoginEditor = mLoginPref.edit();
    }

    public void doLogin(final String email, final String password, final String GCMRegId, final LoginListener loginListener) {
        String params = Urls.LOGIN_1_EMAIL_ID + email + Urls.LOGIN_2_PASSWORD + password + Urls.LOGIN_3_GCM_ID + GCMRegId;
        new WebServiceTask(mContext, params, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                JSONObject response = new JSONObject();
                if (responseObject.getResponseCode() == 200) {
                    Logger.showInfo(TAG, responseObject.getResponse());
                    response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    UserInfo userInfo = new UserInfo(response);
                    if (userInfo.getError().equals("OWCE00")) {      //Success
                        mLoginEditor.putString(Constants.PREF_USERNAME, email);
                        mLoginEditor.putString(Constants.PREF_PASSWORD, password);
                        mLoginEditor.putBoolean(Constants.PREF_ISLOGGED, true);
                        mLoginEditor.putBoolean(Constants.KEY_FROMFB, false);
                        mLoginEditor.putString(Constants.PREF_USERDETAILS, responseObject.getResponse());
                        mLoginEditor.commit();
                        ApplicationEx.getAppInstance().setmUserInfo(userInfo);
                        loginListener.onLoginCompleted(true, userInfo);
                    } else {                                              //Failure
                        loginListener.onLoginCompleted(false, null);
                    }
                } else {
                    loginListener.onLoginCompleted(false, null);
                }
            }
        }).execute();
    }

    public void doFBLogin(final String email, final String GCMRegId, final LoginListener loginListener) {
        String params = Urls.FACEBOOK_LOGIN + email + Urls.FACEBOOK_LOGIN_1 + GCMRegId;
        new WebServiceTask(mContext, params, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                if (responseObject.getResponseCode() == 200) {
                    JSONObject response = new JSONObject();
                    Logger.showInfo(TAG, responseObject.getResponse());
                    response = mCatchJSONExceptions.getJSONFromString(responseObject.getResponse());
                    UserInfo userInfo = new UserInfo(response);
                    if (userInfo.getError().equals("OWCE00")) {                                     //Success
                        mLoginEditor.putString(Constants.PREF_USERNAME, email);
                        mLoginEditor.putBoolean(Constants.PREF_ISLOGGED, true);
                        mLoginEditor.putBoolean(Constants.KEY_FROMFB, true);
                        mLoginEditor.putString(Constants.PREF_USERDETAILS, responseObject.getResponse());
                        mLoginEditor.commit();
                        ApplicationEx.getAppInstance().setmUserInfo(userInfo);
                        loginListener.onLoginCompleted(true, userInfo);
                    } else {
                        userInfo.setError("0WCE02");                                                //For Registration
                        loginListener.onLoginCompleted(false, userInfo);
                    }
                } else {
                    UserInfo userInfo = new UserInfo();                                             //Failure
                    userInfo.setError("0WCE02");
                    loginListener.onLoginCompleted(false, userInfo);
                }
            }
        }).execute();
    }

}
