package aynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import listeners.WSListener;
import model.ResponseObject;

/**
 * Created by SaraschandraaM on 12/08/16.
 */
public class UploadImageTask extends AsyncTask<Object, Void, ResponseObject> {

    Context mContext;
    String ImagePath, UserId, sURL = "http://quiz.thedevelopersolutions.com/api1/upload.php?";
    WSListener wsListener;

    public UploadImageTask(Context mContext, String ImagePath, String UserId, WSListener wsListener) {
        this.mContext = mContext;
        this.ImagePath = ImagePath;
        this.UserId = UserId;
        this.wsListener = wsListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (wsListener != null) {
            wsListener.onRequestSent();
        }
    }

    @Override
    protected ResponseObject doInBackground(Object... objects) {
        String fileName = ImagePath;
        ResponseObject responseObject = new ResponseObject();
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(ImagePath);

        String mStrUrl = "strarun";
        String mStrReturn = "";

        try {

            // open a URL connection to the Servlet
            FileInputStream fileInputStream = new FileInputStream(sourceFile);

            String loadurl = sURL + "&user_id=" + UserId;// url

            Log.d("Image", "Center loadurl " + loadurl);

            URL url = new URL(loadurl);

            Log.d("<<<<<<Profile>>>>>>>>", "id with url in async--------->" + loadurl);

            // Open a HTTP connection to the URL
            conn = (HttpURLConnection) url.openConnection();

            Log.d("<<<Profile>>>", "successfully crossed2");

            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            Log.d("<<<Profile>>>", " successfullcrossed3");
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("file", fileName);

            dos = new DataOutputStream(conn.getOutputStream());
            Log.d("<<<Profile>>>", " successfully crossed4");
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition:form-data; name=\"file\"; filename=\"image.jpg\""
                    + lineEnd);

            dos.writeBytes(lineEnd);

            // create a buffer of maximum size
            bytesAvailable = fileInputStream.available();

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
            Log.d("<<<Profile>>>", " successfully crossed6");
            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            Log.d("<<<Profile>>", " successfully crossed7");
            // Responses from the server (code and message)
            // serverResponseCode = conn.getResponseCode();

            // String json =dos.
            // Log.d("", "OWC Response---------->" + json);
            String serverResponseMessage = conn.getResponseMessage();
            Log.d("<<<Profile>>>", " successfully crossed8");

            Log.i("uploadFile","HTTP Response:" + serverResponseMessage + ": " + serverResponseMessage);

            DataInputStream inStream = new DataInputStream(conn.getInputStream());

            Log.d("<<Profile>>>", " successfully crossed ");
            // String str;

            while ((mStrUrl = inStream.readLine()) != null) {
                Log.e("Debug", "Server Response for OWC-------> " + mStrUrl);
                if (!mStrUrl.equalsIgnoreCase("owc029")) {

//                    prefLoginResponse = ProfileActivity.this
//                            .getSharedPreferences("prefs_login_response",
//                                    MODE_WORLD_READABLE);
//
//                    prefLoginResponseEditor = prefLoginResponse.edit();
//                    prefLoginResponseEditor.putString("image", mStrUrl);
//                    prefLoginResponseEditor.commit();

                    Log.d("Debug", "uploadFile Server Respone for OWC After pref-------> " + mStrUrl);
                    Log.e("", "Center Respone " + mStrUrl);
                    mStrReturn = mStrUrl;

                }

            }
            responseObject.setResponseCode(conn.getResponseCode());
            responseObject.setResponse(mStrReturn);
            inStream.close();

            // close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();

        } catch (MalformedURLException ex) {

            ex.printStackTrace();

            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
        } catch (Exception e) {

            e.printStackTrace();

            // Log.e("Upload file to server Exception","Exception : " + e.getMessage(), e.printStackTrace());

        }

        return responseObject;
    }

    @Override
    protected void onPostExecute(ResponseObject responseObject) {
        super.onPostExecute(responseObject);
        if (wsListener != null) {
            wsListener.onResposneReceived(responseObject);
        }
    }
}
