package aynctask;

import android.content.Context;

import listeners.WSListener;
import model.ResponseObject;

/**
 * Created by SaraschandraaM on 12/08/16.
 */
public class WS_UploadImage {

    Context mContext;

    public interface UploadImageListener {
        public void onImageUploading();

        public void onImageUploaded(boolean isSuccess, String ImageURL);
    }

    public WS_UploadImage(Context mContext) {
        this.mContext = mContext;
    }

    public void doUploadImage(String ImagePath, String UserId, final UploadImageListener uploadImageListener) {
        new UploadImageTask(mContext, ImagePath, UserId, new WSListener() {
            @Override
            public void onRequestSent() {
                uploadImageListener.onImageUploading();
            }

            @Override
            public void onResposneReceived(ResponseObject responseObject) {
                uploadImageListener.onImageUploaded(true, responseObject.getResponse());
            }
        }).execute();
    }
}
